const gQUERY_STRING = window.location.search;
const gURL_PARAMS = new URLSearchParams(gQUERY_STRING);
const gID = gURL_PARAMS.get("id");

var gQuantityOrder = 1;
var gItem = null;

var gCart = [];
var gPage = 1;

var gUSERNAME = "";
var gUserPassword = "";
var gUserId = 0;
var gUserComment = {
    rating: 0,
    comment: "",
    users: { id: 0 },
    book: { id: 0 }
}
var gBookComment;
var gRatingDetail;

function getDomainName(from = window.location.href) {
    var addr = from;
    var start = addr.indexOf('/') + 2;
    var end = addr.indexOf('/', start);
    return addr.substr(start, end != -1 ? end - start : addr.length - start);
}

function getUserNameInfo() {
    var userNameTemp = localStorage.getItem('username');
    var userPasswordTemp = localStorage.getItem("password");
    var userIdTemp = localStorage.getItem("id");

    if (userNameTemp == null || userPasswordTemp == null || userIdTemp == null) return;
    gUSERNAME = userNameTemp;
    gUserPassword = userPasswordTemp;
    gUserId = parseInt(userIdTemp);
}

function getUserNameLoginButton() {
    if (gUSERNAME == "") return;
    $("#login-button").addClass("dropdown");
    $("#login-button").html(
        `
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        👤
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item">Xin chào, ${gUSERNAME}</a>
          <a class="dropdown-item" href="My Order History.html">Đơn hàng của tôi</a> 
          <a class="dropdown-item" id= "signout-button" href="Product Category Page.html">Đăng xuất</a>
        </div>
        `
    )
    $("#signout-button").click(function () {
        localStorage.removeItem("username");
        localStorage.removeItem("password");
        location.reload();
    })

}

function getCartItems() {
    var cartJson = localStorage.getItem('cart');
    if (cartJson == null) return;
    gCart = JSON.parse(cartJson);
}

function saveCartItems() {
    console.log(gCart);
    localStorage.setItem('cart', JSON.stringify(gCart));
}

function addToCart() {
    if (gItem == null) return;
    $("#quantity-order").val(1);
    for (let i = 0; i < gCart.length; i++) {
        if (gCart[i].id == gItem.id) { // if item already exist in cart
            gCart[i].quantity += gQuantityOrder;
            gQuantityOrder = 1;
            saveCartItems();
            updateCartIcon();
            alert('Đã thêm sản phẩm vào giỏ hàng');
            return;
        }
    }
    //if not exist
    gItem.quantity = gQuantityOrder; // set item quantity
    gCart.push(gItem); // add to cart
    gQuantityOrder = 1;
    saveCartItems();
    updateCartIcon();
    alert('Đã thêm sản phẩm vào giỏ hàng');
}

function updateCartIcon() {
    if (gCart.length == 0) {
        $('#cart-item').hide();
    } else {
        $('#cart-item').show();
        $('#cart-item').html(gCart.length);
    }
}

function increaseDecreaseQuantity(amount) {
    if ((gQuantityOrder + amount) < 1)
        return;

    gQuantityOrder += amount;
    $("#quantity-order").val(gQuantityOrder);
}

function getStarFromValue(num) {
    ratingStar = ''
    for (var i = 1; i <= 5; i++) {
        if (i > num) ratingStar += `☆`;
        else ratingStar += `★`;
    }
    return ratingStar;
}

function setRating(num) {
    gUserComment.rating = num;
    console.log(gUserComment.rating);
    ratingStar = ''
    for (var i = 1; i <= 5; i++) {
        if (i > gUserComment.rating) ratingStar += `<span onclick="setRating(${i})">☆</span>`;
        else ratingStar += `<span onclick="setRating(${i})">★</span>`;
    }
    $('#user-rating').html(ratingStar);
}

function checkBookUserOrder() {
    $.ajax({
        url: `http://${getDomainName()}:8080/user/checkbook/${gUserId}/${gID}`,
        type: 'GET',
        dataType: "json",
        success: function (pRes) {
            console.log(pRes);
            if (pRes == true) {
                $("#write-comment-button").show();
                $("#write-comment-button").click(function () {
                    $("#comment-write").slideDown();
                    $("#send-comment-button").click(function () {
                        gUserComment.comment = $("#user-comment").val();
                        gUserComment.users.id = gUserId;
                        gUserComment.book.id = gID;
                        $.ajax({
                            url: `http://${getDomainName()}:8080/ratingcomment/add`,
                            type: 'POST',
                            dataType: "json",
                            data: JSON.stringify(gUserComment),
                            contentType: "application/json;charset=utf-8",
                            success: function (pRes) {
                                console.log("Đăng nhận xét thành công");
                                console.log(pRes);
                                alert("Đăng nhận xét thành công");
                                getAllUserCommentPerPage();
                                $('#write-comment-button').hide();
                                $('#comment-write').slideUp();
                                $.getJSON(`http://${getDomainName()}:8080/avgbookrating/${gID}`, {}, function (res) {
                                    //book-rating-star
                                    $('#book-rating-star').html(`${getStarFromValue(res.avgRating)} <span style="color:gray;font-size:1rem">(${res.avgRating}/5 điểm - ${res.userCount} người đánh giá)</span>`)
                                });
                            },
                            error: function (pAjaxContext) {
                                console.log(pAjaxContext.responseText);
                            }
                        });
                    })
                });
            }
        },
        error: function (pAjaxContext) {
            console.log(pAjaxContext.responseText);
        }
    })
}

function updateComment() {
    listComment = '';
    if (gBookComment.data.length == 0) { $("#comment-rating-card").hide(); return };
    for (let comment of gBookComment.data) {
        listComment += `
        <div class="col-sm-12">
            <span class="font-weight-bold mr-2">${comment.userName}</span>
            <span style="color: chartreuse; font-size: 1.75rem">${getStarFromValue(comment.rating)}</span><br>
            <span class="text-secondary">${new Date(comment.createdDate).toLocaleString('vi-VN')}</span><br>
            <p>${comment.comment}</p>
        </div>
        `;
    }
    $('#comment-list').html(listComment);
    pageList = '';
    for (let i = 0; i < gBookComment.pageCount; i++) {
        if (i + 1 == gPage) {
            pageList += `
                <button type="button" class="btn btn-primary">${i + 1}</button>
            `;
        } else {
            pageList += `
                <button type="button" class="btn btn-light" onclick="setPage(${i + 1})">${i + 1}</button>
            `;
        }
    }
    prevOnclick = gPage - 1 <= 0 ? '' : `onclick="setPage(${gPage - 1})"`;
    nextOnclick = gPage + 1 > gBookComment.pageCount ? '' : `onclick="setPage(${gPage + 1})"`;

    $('#page-button').html(`
        <button type="button" class="btn btn-info" ${prevOnclick}>«</button>
        ${pageList}
        <button type="button" class="btn btn-info" ${nextOnclick}>»</button>
    `);
}

function setPage(num) {
    gPage = num;
    getAllUserCommentPerPage();
}

function getUserRatingDetail() {
    var sumRating = 0;
    $.ajax({
        url: `http://${getDomainName()}:8080/ratingdetail/${gID}`,
        type: 'GET',
        dataType: "json",
        success: function (pRes) {
            console.log(pRes);
            gRatingDetail = pRes;
            for (let i of pRes) sumRating += i.ratingCount;
            for (let i of pRes) {
                $(`#star${i.ratingScore}-rating`).css("width", `${(i.ratingCount/sumRating)*100}%`);
                $(`#star${i.ratingScore}-count`).html(`${i.ratingCount}`);
            }
        },
        error: function (pAjaxContext) {
            console.log(pAjaxContext.responseText);
        }
    })
}

function getAllUserCommentPerPage() {
    $.ajax({
        url: `http://${getDomainName()}:8080/ratingcomment/${gID}?page=${gPage}`,
        type: 'GET',
        dataType: "json",
        success: function (pRes) {
            console.log(pRes);
            gBookComment = pRes;
            updateComment();
            $.getJSON(`http://${getDomainName()}:8080/ratingcomment/user/${gUserId}/${gID}`, {}, function (res) {
                if (res) {
                    $('#write-comment-button').hide();
                }
            });
        },
        error: function (pAjaxContext) {
            console.log(pAjaxContext.responseText);
        }
    })
}

$(document).ready(function () {
    $("#comment-write").hide();
    $("#write-comment-button").hide();
    getUserNameInfo();
    getUserNameLoginButton();
    var detailImage = "";
    getCartItems();
    updateCartIcon();
    checkBookUserOrder();
    getAllUserCommentPerPage();
    getUserRatingDetail();
    $.getJSON(`http://${getDomainName()}:8080/avgbookrating/${gID}`, {}, function (res) {
        //book-rating-star
        $('#book-rating-star').html(`${getStarFromValue(res.avgRating)} <span style="color:gray;font-size:1rem">(${res.avgRating}/5 điểm - ${res.userCount} người đánh giá)</span>`)
    });
    $.ajax({
        url: `http://${getDomainName()}:8080/books/` + gID,
        type: 'GET',
        dataType: "json",
        success: function (pRes) {
            console.log(pRes);
            gItem = pRes;
            $("#product-breadcrumb").html(pRes.language);
            var imageDetail = pRes.bookDetailsImage.split(";");
            for (let i of imageDetail) {
                detailImage += `
                    <div class="col-4">
                        <img class="detail-img" src="${i}" style="width: 100px;">
                    </div>
                                `;
            }

            $("#image-detail-add").html(detailImage);
            $("#show-img").prop("src", pRes.bookImage);
            $("#book-title").html(pRes.bookTitle);
            $("#book-publisher").html("Nhà xuất bản: " + pRes.bookPublisher);
            $("#book-author").html("Tác giả: " + pRes.author);
            $("#book-price").html(pRes.bookPrice.toLocaleString('vi-VN') + "đ");
            $("#book-description").html(pRes.bookDecription);
            $(".detail-img").on("click", function () {
                console.log(`clicked on ${$(this).prop('src')}`)
                $("#show-img").prop("src", $(this).prop('src'));
            });
        },
        error: function (pAjaxContext) {
            console.log(pAjaxContext.responseText);
        }
    })
    $.ajax({
        url: `http://${getDomainName()}:8080/books/randomsixsample`,
        type: 'GET',
        dataType: "json",
        success: function (pRes) {
            console.log(pRes);
            var product = "";
            for (let i of pRes) {
                product +=
                    `<div class="col-sm-4">
                    <a href="Product%20Detail.html?id=${i.id}">           
                        <img src="${i.bookImage}" class="product-img">
                        <p style="text-align: center;">${i.bookTitle}</p>
                        <h3 style="text-align: center;color:chartreuse">${i.bookPrice.toLocaleString('vi-VN')}đ</h3>
                    </a>         
                 </div>
                 `;
            }
            $("#six-sample-product").html(product);
        },
        error: function (pAjaxContext) {
            console.log(pAjaxContext.responseText);
        }
    })
})