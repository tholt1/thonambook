var gCart = [];
gUserInfo = {
    userName: "",
    password: "",
    fullName: "",
    email: "",
    phoneNumber: "",
    region: "",
    cityState: "",
    address: "",
    payment: 0,
    role: 1,
    register: 1,
    orders: []
}

var gUSERNAME = "";
var gUserPassword = "";

function getUserNameInfo(){
    var userNameTemp = localStorage.getItem('username');
    var userPasswordTemp = localStorage.getItem("password");
    if (userNameTemp == null || userPasswordTemp == null) return;
    gUSERNAME = userNameTemp;
    gUserPassword = userPasswordTemp;
}

function getDomainName(from = window.location.href) {
    var addr = from;
    var start = addr.indexOf('/') + 2;
    var end = addr.indexOf('/', start);
    return addr.substr(start, end!=-1 ? end - start:addr.length - start);
}

function getUserNameLoginButton(){
    if (gUSERNAME == "") return;
    $("#login-button").addClass("dropdown");
    $("#login-button").html(
        `
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        👤
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item">Xin chào, ${gUSERNAME}</a>
          <a class="dropdown-item" href="My Order History.html">Đơn hàng của tôi</a> 
          <a class="dropdown-item" id= "signout-button" href="Main page - Customer.html">Đăng xuất</a>
        </div>
        `
    )
        $("#signout-button").click(function(){
            localStorage.removeItem("username");
            localStorage.removeItem("password");
            location.reload();
        })

}

function getCartItems() {
    var cartJson = localStorage.getItem('cart');
    if (cartJson == null) return;
    gCart = JSON.parse(cartJson);
}

function saveCartItems() {
    console.log(gCart);
    localStorage.setItem('cart', JSON.stringify(gCart));
}

function updateCartIcon() {
    if (gCart.length == 0) {
        $('#cart-item').hide();
    } else {
        $('#cart-item').show();
        $('#cart-item').html(gCart.length);
    }
}

function validateInfo() {
    var result = true;
    if (gUserInfo.fullName == "") {
        $("#name-input").addClass("is-invalid");
        result = false;
    }
    if (gUserInfo.email == "" || gUserInfo.email.indexOf("@") == -1) {
        $("#email-input").addClass("is-invalid");
        result = false;
    }
    if (gUserInfo.phoneNumber == "") {
        $("#phone-input").addClass("is-invalid");
        result = false;
    }
    if (gUserInfo.region == "") {
        $("#region-input").addClass("is-invalid");
        result = false;
    }
    if (gUserInfo.cityState == "") {
        $("#city-state-input").addClass("is-invalid");
        result = false;
    }
    if (gUserInfo.address == "") {
        $("#address-input").addClass("is-invalid");
        result = false;
    }
    return result;
}

function getUserInfo() {
    gUserInfo.fullName = $("#name-input").val().trim();
    gUserInfo.email = $("#email-input").val().trim();
    gUserInfo.phoneNumber = $("#phone-input").val().trim();
    gUserInfo.region = $("#region-input").val().trim();
    gUserInfo.cityState = $("#city-state-input").val().trim();
    gUserInfo.address = $("#address-input").val().trim();
}

function updateUserInfo(){
    if (!validateInfo()) return;
    getUserInfo();
    $.ajax({
        url: `http://${getDomainName()}:8080/users/update/` + gUserInfo.id,
        type: 'PUT',
        dataType: "json",
        data: JSON.stringify(gUserInfo),
        contentType: "application/json;charset=utf-8",
        success: function (pRes) {
            console.log(pRes);
        },
        error: function (pAjaxContext) {
            console.log(pAjaxContext.responseText);
        }
    });
}

function getUserInfoByNameAndPassword(){
    $.ajax({
        url: `http://${getDomainName()}:8080/users/findusernamepassword/${gUSERNAME}/${gUserPassword}`,
        type: 'GET',
        dataType: "json",
        success: function (pRes) {
            console.log(pRes);
            $("#username-input").val(pRes.userName);
            $("#name-input").val(pRes.fullName);
            $("#phone-input").val(pRes.phoneNumber);
            $("#email-input").val(pRes.email);
            $("#region-input").val(pRes.region);
            $("#city-state-input").val(pRes.cityState);
            $("#address-input").val(pRes.address);
            $("#edit-button").prop("disabled", false);
            gUserInfo = pRes;
        },
        error: function (pAjaxContext) {
            console.log(pAjaxContext.responseText);
        }
    })
}


$(document).ready(function () {
    getUserNameInfo();
    getUserNameLoginButton();
    getUserInfoByNameAndPassword();
    getCartItems();
    updateCartIcon();
    $("#edit-button").click(function(){
        alert("Cập nhật dữ liệu thành công!");
        updateUserInfo();
    })
})