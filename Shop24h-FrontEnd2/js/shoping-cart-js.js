var gCart = [];
var gVoucher = null;
var gCustomerPhoneNumber = [];

var gCustomerInfo = {
    fullName: "",
    phoneNumber: "",
    email: "",
    region: "",
    cityState: "",
    payment: 0,
    address: "",
    userName: "",
    password: "",
    role: 0,
    register: 0,
    orders: [],
}

var gUSERNAME = "";
var gUserPassword = "";
var gUserId = 0;

function getDomainName(from = window.location.href) {
    var addr = from;
    var start = addr.indexOf('/') + 2;
    var end = addr.indexOf('/', start);
    return addr.substr(start, end!=-1 ? end - start:addr.length - start);
}

function getUserNameInfo(){
    var userNameTemp = localStorage.getItem('username');
    var userPasswordTemp = localStorage.getItem("password");
    var userIdTemp = localStorage.getItem("id");

    if (userNameTemp == null || userPasswordTemp == null || userIdTemp == null) return;
    gUSERNAME = userNameTemp;
    gUserPassword = userPasswordTemp;
    gUserId = parseInt(userIdTemp);
}

function getUserNameLoginButton(){
    if (gUSERNAME == "") return;
    $("#login-button").addClass("dropdown");
    $("#login-button").html(
        `
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        👤
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item">Xin chào, ${gUSERNAME}</a>
          <a class="dropdown-item" href="My Order History.html">Đơn hàng của tôi</a> 
          <a class="dropdown-item" id= "signout-button" href="Product Category Page.html">Đăng xuất</a>
        </div>
        `
    )
        $("#signout-button").click(function(){
            localStorage.removeItem("username");
            localStorage.removeItem("password");
            location.reload();
        })

}

function getUserInfoByNameAndPassword(){
    $.ajax({
        url: `http://${getDomainName()}:8080/users/findusernamepassword/${gUSERNAME}/${gUserPassword}`,
        type: 'GET',
        dataType: "json",
        success: function (pRes) {
            console.log(pRes);
            $("#name-input").val(pRes.fullName);
            $("#phone-input").val(pRes.phoneNumber);
            $("#email-input").val(pRes.email);
            $("#region-input").val(pRes.region);
            $("#state-input").val(pRes.cityState);
            $("#address-input").val(pRes.address);
            $("#name-input").prop('readonly', true);
            $("#phone-input").prop('readonly', true);
            $("#email-input").prop('readonly', true);
            $("#region-input").prop('readonly', true);
            $("#state-input").prop('readonly', true);
            $("#address-input").prop('readonly', true);
            gCustomerInfo.userName = pRes.userName;
            gCustomerInfo.password = pRes.password;
            gCustomerInfo.role = pRes.role;
            gCustomerInfo.register = pRes.register;
        },
        error: function (pAjaxContext) {
            console.log(pAjaxContext.responseText);
        }
    })
}

function getCustomerInfo() {
    gCustomerInfo.fullName = $("#name-input").val().trim();
    gCustomerInfo.phoneNumber = $("#phone-input").val().trim();
    gCustomerInfo.email = $("#email-input").val().trim();
    gCustomerInfo.region = $("#region-input").val().trim();
    gCustomerInfo.cityState = $("#state-input").val().trim();
    gCustomerInfo.address = $("#address-input").val().trim();

    if (gCustomerInfo.fullName == "" || gCustomerInfo.phoneNumber == "" ||
        gCustomerInfo.email == "" || gCustomerInfo.email.indexOf("@") == -1 || gCustomerInfo.region == "" ||
        gCustomerInfo.cityState == "" || gCustomerInfo.address == "") {
        alert("Nhập sai dữ liệu");
        return false;
    }
    return true;
}

function getCartItems() {
    var cartJson = localStorage.getItem('cart');
    if (cartJson == null) return;
    gCart = JSON.parse(cartJson);
}

function saveCartItems() {
    console.log(gCart);
    localStorage.setItem('cart', JSON.stringify(gCart));
}

function updateCartIcon() {
    if (gCart.length == 0) {
        $('#cart-item').hide();
    } else {
        $('#cart-item').show();
        $('#cart-item').html(gCart.length);
    }
}

function increaseDecreaseQuantity(index, amount) {
    if ((gCart[index].quantity + amount) < 1)
        return;

    gCart[index].quantity += amount;
    $("#quantity-order-" + index).val(gCart[index].quantity);
    $("#price-total-" + index).html((gCart[index].bookPrice * gCart[index].quantity).toLocaleString('vi-VN') + "đ");
    saveCartItems();
    updateFinalPayment();
}

function removeCartItem(index) {
    if (confirm('Bạn có muốn xóa sản phẩm này khỏi giỏ hàng?')) {
        gCart.splice(index, 1);
        saveCartItems();
        updateCartTable();
        updateCartIcon();
    }
}

function getVoucherDiscount(sum) {
    if (gVoucher != null) {
        return sum * gVoucher.phanTramGiamGia / 100;
    }
    return 0;
}

function getTotalPrice() {
    var sum = 0;
    for (let i of gCart) {
        sum += i.bookPrice * i.quantity;
    }
    return sum;
}

function getPayment() {
    var sum = getTotalPrice();
    return sum - getVoucherDiscount(sum);
}

function checkVoucher() {
    var voucherInput = $("#voucher-input").val();
    $('#voucher-message').html(``);
    $.ajax({
        url: `http://${getDomainName()}:8080/voucher/find/` + voucherInput,
        type: 'GET',
        dataType: "json",
        success: function (pRes) {
            console.log(pRes);
            if (pRes != null) {
                gVoucher = pRes;
                updateFinalPayment();
            }
        },
        error: function (pAjaxContext) {
            console.log(pAjaxContext.responseText);
            gVoucher = null;
            $('#voucher-message').html(`Không hợp lệ`);
            updateFinalPayment();
        }
    })
}

function addOrder() {
    var orderDetail = [];
    for (let i of gCart) {
        orderDetail.push({
            quantityOrder: i.quantity,
            book: i
        });
    }
    gCustomerInfo.orders.push({
        comments: "",
        //orderDate: "2021-06-18T08:26:18.000+00:00",
        status: "Cho kiem duyet",
        voucher: gVoucher,
        payment: getTotalPrice(),
        orderDetails: orderDetail
    });
}

function updateCustomer() {
    if (!getCustomerInfo()) return;
    addOrder();
    gCustomerInfo.payment += getPayment();
    $.ajax({
        url: `http://${getDomainName()}:8080/users/update/` + gCustomerInfo.id,
        type: 'PUT',
        dataType: "json",
        data: JSON.stringify(gCustomerInfo),
        contentType: "application/json;charset=utf-8",
        success: function (pRes) {
            console.log(pRes);
            gCart = [];
            saveCartItems();
            alert('Dat hang thanh cong');
            window.location.href = "Main page - Customer.html";
        },
        error: function (pAjaxContext) {
            console.log(pAjaxContext.responseText);
        }
    });
}

function addCustomer() {
    if (!getCustomerInfo()) return;
    gCustomerInfo.orders = [];
    gCustomerInfo.payment = getPayment();
    addOrder();
    $.ajax({
        url: `http://${getDomainName()}:8080/users/add`,
        type: 'POST',
        dataType: "json",
        data: JSON.stringify(gCustomerInfo),
        contentType: "application/json;charset=utf-8",
        success: function (pRes) {
            console.log(pRes);
            gCart = [];
            saveCartItems();
            alert('Dat hang thanh cong');
            window.location.href = "Main page - Customer.html";
        },
        error: function (pAjaxContext) {
            console.log(pAjaxContext.responseText);
        }
    });
}

function saveCustomerOrder() {
    var phoneInput = $('#phone-input').val();
    $.ajax({
        url: `http://${getDomainName()}:8080/users/findphone/` + phoneInput,
        type: 'GET',
        dataType: "json",
        success: function (pRes) {
            console.log(pRes);
            //getCustomerInfo();
            gCustomerInfo = pRes;
            updateCustomer();
        },
        error: function (pAjaxContext) {
            console.log(pAjaxContext.responseText);
            addCustomer();
        }
    });
}

function updateFinalPayment() {
    var totalprice = getTotalPrice();
    $('#voucher-discount').html(`-${getVoucherDiscount(totalprice).toLocaleString('vi-VN')}đ`);
    $('#final-payment').html((totalprice - getVoucherDiscount(totalprice)).toLocaleString('vi-VN') + "đ");
}

function updateCartTable() {
    var count = 1;
    getCartItems();
    var product = "";
    for (let i of gCart) {
        product +=
            `
        <tr>
            <td>${count}</td>
            <td><img src="${i.bookImage}" class="product-img"></td>
            <td>${i.bookTitle}</td>
            <td class="text-right">${i.bookPrice.toLocaleString('vi-VN')}đ</td>
            <td>
                <div class="input-group form-inline mb-3" style="width: 130px;">
                    <div class="input-group-prepend">
                        <button class="btn btn-outline-success" type="button" id="descrease-quantity"
                            onclick="increaseDecreaseQuantity(${count - 1},-1)">ー</button>
                    </div>
                    <input type="text" class="form-control text-center" value="${i.quantity}" aria-label=""
                        aria-describedby="basic-addon1" id="quantity-order-${count - 1}" readonly>
                    <div class="input-group-append">
                        <button class="btn btn-outline-success" type="button" id="increase-quantity"
                            onclick="increaseDecreaseQuantity(${count - 1},1)">✙</button>
                    </div>
                </div>
            </td>
            <td id="price-total-${count - 1}" class="text-right">${(i.bookPrice * i.quantity).toLocaleString('vi-VN')}đ</td>
            <td><button class="btn btn-danger" onclick="removeCartItem(${count - 1})">X</button></td>
        </tr>
        `;
        count++;
    }
    $("#my-cart-table").html(`
    <thead>
        <tr>
            <th>STT</th>
            <th>Ảnh</th>
            <th>Tên sách</th>
            <th>Đơn giá</th>
            <th>Số lượng</th>
            <th>Thành tiền</th>
            <th>Xóa</th>
        </tr>
    </thead>
    <tbody>
        ${product};
        <tr>
            <td colspan="3" class="text-right">Voucher</td>
            <td colspan="2">
            <input id="voucher-input" type="text" class="form-control d-inline-block" style="width: 65%">
            <button class="btn btn-success" id="check-voucher" onclick="checkVoucher()">Check</button>
            <p id="voucher-message" class="text-danger"></p>
            </td>
            <td id="voucher-discount" class="text-right">
                
            </td>
            <td></td>
        </tr>
        <tr class="font-weight-bold">
            <td colspan="5">Thành tiền</td>
            <td id="final-payment" class="text-right"></td>
            <td></td>
        </tr>
    </tbody>
            `);
    updateFinalPayment();
}

$(document).ready(function () {
    getUserNameInfo();
    getUserNameLoginButton();
    getUserInfoByNameAndPassword();
    $("#customer-info").hide();
    $("#payment-button").on("click", function () {
        $("#customer-info").show();
        // getCustomerInfo();

    })
    updateCartTable();
    updateCartIcon();
})
