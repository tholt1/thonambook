var gCart = [];

var gUSERNAME = "";
var gUserPassword = "";
var gUserId = 0;

function getUserNameInfo(){
    var userNameTemp = localStorage.getItem('username');
    var userPasswordTemp = localStorage.getItem("password");
    var userIdTemp = localStorage.getItem("id");

    if (userNameTemp == null || userPasswordTemp == null || userIdTemp == null) return;
    gUSERNAME = userNameTemp;
    gUserPassword = userPasswordTemp;
    gUserId = parseInt(userIdTemp);
}

function getDomainName(from = window.location.href) {
    var addr = from;
    var start = addr.indexOf('/') + 2;
    var end = addr.indexOf('/', start);
    return addr.substr(start, end!=-1 ? end - start:addr.length - start);
}


function getUserNameLoginButton(){
    if (gUSERNAME == "") return;
    $("#login-button").addClass("dropdown");
    $("#login-button").html(
        `
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        👤
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item">Xin chào, ${gUSERNAME}</a>
          <a class="dropdown-item" href="My Order History.html">Đơn hàng của tôi</a> 
          <a class="dropdown-item" id= "signout-button" href="Main page - Customer.html">Đăng xuất</a>
        </div>
        `
    )
        $("#signout-button").click(function(){
            localStorage.removeItem("username");
            localStorage.removeItem("password");
            location.reload();
        })

}

function getCartItems() {
    var cartJson = localStorage.getItem('cart');
    if (cartJson == null) return;
    gCart = JSON.parse(cartJson);
}

function saveCartItems() {
    console.log(gCart);
    localStorage.setItem('cart', JSON.stringify(gCart));
}

function updateCartIcon() {
    if (gCart.length == 0) {
        $('#cart-item').hide();
    } else {
        $('#cart-item').show();
        $('#cart-item').html(gCart.length);
    }
}

$(document).ready(function(){
    getUserNameInfo();
    getUserNameLoginButton();
    getCartItems();
    updateCartIcon();
    $.ajax({
        url: `http://${getDomainName()}:8080/books/order`,
        type: 'GET',
        dataType: "json",
        success: function (pRes) {
            console.log(pRes);
            var product ="";
            for (let i of pRes){
                product +=
                    `<div class="col-sm-3">
                        <a href="Product%20Detail.html?id=${i.id}">
                            <img src="${i.bookImage}" class="product-img">
                            <p style="text-align: center;">${i.bookTitle}</p>
                            <h3 style="text-align: center;color:chartreuse">${i.bookPrice.toLocaleString('vi-VN')}đ</h3>
                        </a>
                    </div>`;
                

            }
            $("#latest-product").html(product);

        },
        error: function (pAjaxContext) {
            console.log(pAjaxContext.responseText);
        }
    })

    $.ajax({
        url: `http://${getDomainName()}:8080/books/random`,
        type: 'GET',
        dataType: "json",
        success: function (pRes) {
            console.log(pRes);
            var product ="";
            var cnt = 1;
            for (let i of pRes){
                var pclass = cnt == 1 ? "carousel-item active":"carousel-item";
                product +=
                    `<div class="${pclass}">
                    <div class="row">
                        <div class="col-sm-7">
                            <h1 style="text-align: center;">${i.bookTitle} </h1>
                            <div class="book-desc">${i.shortDecription}</div>
                            <div class="text-center my-3"><a class="btn btn-danger" href="Product Category Page.html">Shop now!</a></div>
                        </div>
                        <div class="col-sm-5">
                            <img class="d-block w-100" src="${i.bookImage}">
                        </div>
                    </div>
                </div>`;
                
                cnt++;
            }
            $("#carousel-product").html(product);

        },
        error: function (pAjaxContext) {
            console.log(pAjaxContext.responseText);
        }
    })
})