gUserInfo = {
    userName: "",
    password: "",
    fullName: "",
    email: "",
    phoneNumber: "",
    region: "",
    cityState: "",
    address: "",
    payment: 0,
    role: 1,
    register: 1,
    orders: []
}

function getDomainName(from = window.location.href) {
    var addr = from;
    var start = addr.indexOf('/') + 2;
    var end = addr.indexOf('/', start);
    return addr.substr(start, end!=-1 ? end - start:addr.length - start);
}

function validateInfo() {
    var result = true;
    if (gUserInfo.userName == "") {
        $("#username-input").addClass("is-invalid");
        result = false;
    }
    if (gUserInfo.password == "" || $("#confirm-password-input").val() != gUserInfo.password) {
        $("#password-input").addClass("is-invalid");
        $("#confirm-password-input").addClass("is-invalid");
        result = false;
    }
    if (gUserInfo.fullName == "") {
        $("#name-input").addClass("is-invalid");
        result = false;
    }
    if (gUserInfo.email == "" || gUserInfo.email.indexOf("@") == -1) {
        $("#email-input").addClass("is-invalid");
        result = false;
    }
    if (gUserInfo.phoneNumber == "") {
        $("#phone-input").addClass("is-invalid");
        result = false;
    }
    if (gUserInfo.region == "") {
        $("#region-input").addClass("is-invalid");
        result = false;
    }
    if (gUserInfo.cityState == "") {
        $("#city-state-input").addClass("is-invalid");
        result = false;
    }
    if (gUserInfo.address == "") {
        $("#address-input").addClass("is-invalid");
        result = false;
    }
    return result;
}

function getData() {
    gUserInfo.userName = $("#username-input").val().trim();
    gUserInfo.password = $("#password-input").val();
    gUserInfo.fullName = $("#name-input").val().trim();
    gUserInfo.email = $("#email-input").val().trim();
    gUserInfo.phoneNumber = $("#phone-input").val().trim();
    gUserInfo.region = $("#region-input").val().trim();
    gUserInfo.cityState = $("#city-state-input").val().trim();
    gUserInfo.address = $("#address-input").val().trim();
}

function addUser() {
    if (!validateInfo()) return;
    $.ajax({
        url: `http://${getDomainName()}:8080/users/add`,
        type: 'POST',
        dataType: "json",
        data: JSON.stringify(gUserInfo),
        contentType: "application/json;charset=utf-8",
        success: function (pRes) {
            console.log(pRes);
            alert('Đăng ký thành công');
            window.location.href = "Login.html";
        },
        error: function (pAjaxContext) {
            console.log(pAjaxContext.responseText);
        }
    });
}

function findExistUser() {
    gUserInfo.userName = $("#username-input").val().trim();
    $.ajax({
        url: `http://${getDomainName()}:8080/users/findname/` + gUserInfo.userName,
        type: 'GET',
        dataType: "json",
        success: function (pRes) {
            console.log(pRes);
            if (pRes == true) {
                $("#error-message").show();
                $("#user-exist-error").html("Tài khoản đã tồn tại");
                $("#username-input").removeClass("is-valid");
                $("#username-input").addClass("is-invalid");
                $("#signup-button").prop("disabled", true);
            } 
        },
        error: function (pAjaxContext) {
            console.log(pAjaxContext.responseText);
            $("#error-message").hide();
            $("#username-input").removeClass("is-invalid");
            $("#username-input").addClass("is-valid");
            $("#signup-button").prop("disabled", false);
        }
    })
}

$(document).ready(function () {
    $("#error-message").hide();
    $("#check-user-button").click(function () {
        findExistUser();
    })
    $("#signup-button").click(function () {
        getData();
        addUser();
    });
})