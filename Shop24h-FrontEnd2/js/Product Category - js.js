const gQUERY_STRING = window.location.search;
const gURL_PARAMS = new URLSearchParams(gQUERY_STRING);
const gBookType = gURL_PARAMS.get("category") ?? '';
var gPage = parseInt(gURL_PARAMS.get("page") ?? 1) - 1;
var gResult;
var gCart = [];

var gUSERNAME = "";
var gUserPassword = "";
var gUserId = 0;

function getUserNameInfo(){
    var userNameTemp = localStorage.getItem('username');
    var userPasswordTemp = localStorage.getItem("password");
    var userIdTemp = localStorage.getItem("id");

    if (userNameTemp == null || userPasswordTemp == null || userIdTemp == null) return;
    gUSERNAME = userNameTemp;
    gUserPassword = userPasswordTemp;
    gUserId = parseInt(userIdTemp);
}

function getDomainName(from = window.location.href) {
    var addr = from;
    var start = addr.indexOf('/') + 2;
    var end = addr.indexOf('/', start);
    return addr.substr(start, end!=-1 ? end - start:addr.length - start);
}

function getUserNameLoginButton(){
    if (gUSERNAME == "") return;
    $("#login-button").addClass("dropdown");
    $("#login-button").html(
        `
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        👤
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item">Xin chào, ${gUSERNAME}</a>
          <a class="dropdown-item" href="My Order History.html">Đơn hàng của tôi</a> 
          <a class="dropdown-item" id= "signout-button" href="Product Category Page.html">Đăng xuất</a>
        </div>
        `
    )
        $("#signout-button").click(function(){
            localStorage.removeItem("username");
            localStorage.removeItem("password");
            location.reload();
        })

}

function getCartItems() {
    var cartJson = localStorage.getItem('cart');
    if (cartJson == null) return;
    gCart = JSON.parse(cartJson);
}

function saveCartItems() {
    console.log(gCart);
    localStorage.setItem('cart', JSON.stringify(gCart));
}

function updateCartIcon() {
    if (gCart.length == 0) {
        $('#cart-item').hide();
    } else {
        $('#cart-item').show();
        $('#cart-item').html(gCart.length);
    }
}

function updateProduct() {
    $("#all-product").html('');
    var product ="";
    for (let book of gResult.data) {
        product +=`
            <div class="col-sm-4">
                <a href="Product%20Detail.html?id=${book.id}">
                    <img src="${book.bookImage}" class="product-img">
                    <p style="text-align: center;">${book.bookTitle}</p>
                    <h3 style="text-align: center;color:chartreuse">${book.bookPrice.toLocaleString('vi-VN')}đ</h3>
                </a>
            </div>
        `;
    }
    $("#all-product").html(product);
}

function updatePageSelect() {
    $('#page-button').html('');
    pButton = '';
    pageCount = gResult.pageCount;
    for (let i=1;i<=pageCount;i++) {
        if (i == gPage+1) pButton += `<button class="btn btn-primary">${i}</button>`;
        else pButton += `<button onclick="setPage(${i-1})" class="btn btn-light">${i}</button>`;
    }
    $('#page-button').html(`
        <button id="prev-page" class="btn btn-primary">«</button>
        ${pButton}
        <button id="next-page" class="btn btn-primary">»</button>
    `);
    $('#prev-page').click(function() {
        if (gPage > 0) setPage(gPage - 1);
    });
    $('#next-page').click(function() {
        if (gPage + 1 < pageCount) setPage(gPage + 1);
    });
}

function setPage(num) {
    gPage = num;
    updatePageData();
}

const PRICE_RANGE = [
    {min:0, max:2000000000},
    {min:0, max:100000},
    {min:100000, max:200000},
    {min:200000, max:300000},
    {min:300000, max:400000},
    {min:400000, max:2000000000},
];

function getFilterParam() {
    var price = $('input[name="price"]:checked').val();
    var priceRange = PRICE_RANGE[parseInt(price)];
    var publisher = $('input[name="publisher"]:checked').val();
    var language = $('input[name="language"]:checked').val();
    var param = [];
    if (price > 0) param.push(`priceMin=${priceRange.min}&priceMax=${priceRange.max}`);
    if (publisher != '') param.push(`publisher=${publisher}`);
    if (language != '') param.push(`language=${language}`);
    if (gBookType != '') param.push(`category=${gBookType}`);
    param.push(`page=${gPage + 1}`);
    return `?${param.join('&')}`;
}

function updateFilter(reset = false) {
    if (reset) gPage = 0;
    updatePageData();
}

function getData(onSuccess = null) {
    $.ajax({
        url: `http://${getDomainName()}:8080/books/filter${getFilterParam()}`,
        type: 'GET',
        dataType: "json",
        success: function (pRes) {
            console.log(pRes);
            gResult = pRes;
            if (onSuccess != null) onSuccess();
        },
        error: function (pAjaxContext) {
            console.log(pAjaxContext.responseText);
        }
    })
}

function updatePageData() {
    getData(function() {
        updatePageSelect();
        updateProduct();
    });
}

$(document).ready(function(){
    console.log(gPage);
    getUserNameInfo();
    getUserNameLoginButton();
    getCartItems();
    updateCartIcon();
    $('#category-breadcrum').html(gBookType);
    getData(function() {
        updateProduct();
        updatePageSelect();
        $('input[type=radio][name="price"]').click(function() {
            updateFilter(gResult, true);
        });
        $('input[type=radio][name="publisher"]').click(function() {
            updateFilter(gResult, true);
        });
        $('input[type=radio][name="language"]').click(function() {
            updateFilter(gResult, true);
        });
    });
});