var gCart = [];
var gUSERNAME = "";
var gUserPassword = "";

gUserInfo = {
    userName: "",
    password: "",
    fullName: "",
    email: "",
    phoneNumber: "",
    region: "",
    cityState: "",
    address: "",
    payment: 0,
    role: 1,
    register: 1,
    orders: []
}

function getDomainName(from = window.location.href) {
    var addr = from;
    var start = addr.indexOf('/') + 2;
    var end = addr.indexOf('/', start);
    return addr.substr(start, end!=-1 ? end - start:addr.length - start);
}

function getUserNameInfo() {
    var userNameTemp = localStorage.getItem('username');
    var userPasswordTemp = localStorage.getItem("password");
    if (userNameTemp == null || userPasswordTemp == null) return;
    gUSERNAME = userNameTemp;
    gUserPassword = userPasswordTemp;
}

function getUserNameLoginButton() {
    if (gUSERNAME == "") return;
    $("#login-button").addClass("dropdown");
    $("#login-button").html(
        `
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        👤
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item">Xin chào, ${gUSERNAME}</a>
          <a class="dropdown-item" href="My Order History.html">Đơn hàng của tôi</a> 
          <a class="dropdown-item" id= "signout-button" href="Main page - Customer.html">Đăng xuất</a>
        </div>
        `
    )
    $("#signout-button").click(function () {
        localStorage.removeItem("username");
        localStorage.removeItem("password");
        location.reload();
    })

}
function getCartItems() {
    var cartJson = localStorage.getItem('cart');
    if (cartJson == null) return;
    gCart = JSON.parse(cartJson);
}

function saveCartItems() {
    console.log(gCart);
    localStorage.setItem('cart', JSON.stringify(gCart));
}

function updateCartIcon() {
    if (gCart.length == 0) {
        $('#cart-item').hide();
    } else {
        $('#cart-item').show();
        $('#cart-item').html(gCart.length);
    }
}

function getUserInfoByLoginInfo() {
    $.ajax({
        url: `http://${getDomainName()}:8080/users/findusernamepassword/${gUSERNAME}/${gUserPassword}`,
        type: 'GET',
        dataType: "json",
        success: function (pRes) {
            console.log(pRes);
            gUserInfo = pRes;
            fillUserInfo();
            fillOrderTable();
        },
        error: function (pAjaxContext) {
            console.log(pAjaxContext.responseText);
        }
    })
}

function fillUserInfo() {
    $("#name-input").html(gUserInfo.fullName);
    $("#user-input").html(gUserInfo.userName);
    $("#email-input").html(gUserInfo.email);
    $("#region-input").html(gUserInfo.region);
    $("#city-input").html(gUserInfo.cityState);
    $("#address-input").html(gUserInfo.address);
    $("#phone-input").html(gUserInfo.phoneNumber);
}

function fillOrderTable() {
    var allOrders = "";
    var count = 0;
    for (let i of gUserInfo.orders) {
        let orderDate = new Date(i.orderDate);
        allOrders += `
                    <tr>
                    <td>${i.id}</td>
                    <td>${orderDate.toLocaleString()}</td>
                    <td>${i.payment.toLocaleString("vi-VN")}đ</td>
                    <td>${i.status}</td>
                    <td><button class="btn btn-primary" onclick="showOrderDetail(${count})">Xem chi tiết</button></td>
                    </tr>
                    `;
        count++;
    }
    $("#order-table").html(
        `
        <tr>
            <th>Mã đơn</th>
            <th>Thời gian đặt hàng</th>
            <th>Tổng tiền</th>
            <th>Tình trạng</th>
            <th></th>
        </tr>
        ${allOrders}
        `
    )
}

function showOrderDetail(index) {
    var orderInfo = gUserInfo.orders[index];
    var orderDetailsInfo = "";
    var totalPayment = "";
    for (let i = 0; i < orderInfo.orderDetails.length; i++) {
        orderDetailsInfo +=`
                            <tr>
                                <td>${i + 1}</td>
                                <td><img src="${orderInfo.orderDetails[i].book.bookImage}"  class="img-product"></td>
                                <td>${orderInfo.orderDetails[i].book.bookTitle}</td>
                                <td>${orderInfo.orderDetails[i].book.bookPrice.toLocaleString("vi-VN")}đ</td>
                                <td>${orderInfo.orderDetails[i].quantityOrder}</td>
                                <td>${(orderInfo.orderDetails[i].book.bookPrice * orderInfo.orderDetails[i].quantityOrder).toLocaleString("vi-VN")}đ</td>
                            </tr>
                           `;
    }
        totalPayment += `
                        <tr>
                            <td colspan="5" class="text-right"><b>Thành tiền</b></td>
                            <td colspan="5"><b>${orderInfo.payment.toLocaleString("vi-VN")}đ</b></td>
                        </tr>
                        `
    $("#orderdetail-table").show();
    $("#orderdetail-table").html(
        `
                    <tr>
                        <th>STT</th>
                        <th>Ảnh</th>
                        <th>Tên sách</th>
                        <th>Đơn giá</th>
                        <th>Số lượng</th>
                        <th>Thành tiền</th>
                    </tr>
                    ${orderDetailsInfo}
                    ${totalPayment}
        `
    )
}

$(document).ready(function(){
    $("#orderdetail-table").hide();
    getUserNameInfo();
    getUserNameLoginButton();
    getUserInfoByLoginInfo();
    getCartItems();
    updateCartIcon();
    
})

