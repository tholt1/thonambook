var gLoginInfo = {
    userName: "",
    password: "",
}

var gUserId = 0;

var gUserInfo;

function getDomainName(from = window.location.href) {
    var addr = from;
    var start = addr.indexOf('/') + 2;
    var end = addr.indexOf('/', start);
    return addr.substr(start, end!=-1 ? end - start:addr.length - start);
}

function getLoginInfo(){
    gLoginInfo.userName = $("#username-input").val().trim();
    gLoginInfo.password = $("#password-input").val();
}

function getUserLoginInfo(){
    localStorage.setItem("username",gLoginInfo.userName);
    localStorage.setItem("password",gLoginInfo.password);
    localStorage.setItem("id",gUserId);
}

function getUserInfoByLoginInfo(){
    $.ajax({
        url: `http://${getDomainName()}:8080/users/findusernamepassword/${gLoginInfo.userName}/${gLoginInfo.password}`,
        type: 'GET',
        dataType: "json",
        success: function (pRes) {
            console.log(pRes);
            gUserId = pRes.id;
            gUserInfo = pRes;
            getUserLoginInfo();
            if (gUserInfo.role == 1){
                window.location.href = `Main page - Customer.html`;
            } else if (gUserInfo.role == 2){
                window.location.href = `../Shop24h-Admin2/Admin page - Product List.html`;
            }
        },
        error: function (pAjaxContext) {
            console.log(pAjaxContext.responseText);
        }
    })
}

$(document).ready(function(){
    $("#signin-button").click(function(){
        getLoginInfo();
        getUserInfoByLoginInfo();
    });
})