package com.devcamp.thonambook.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.thonambook.entity.Book;
import com.devcamp.thonambook.entity.Order;
import com.devcamp.thonambook.entity.OrderDetail;
import com.devcamp.thonambook.repository.OrderDetailRepository;

@CrossOrigin
@RestController
public class OrderDetailController {

	@Autowired
	private OrderDetailRepository orderDetailRepository;
	
	@GetMapping("/order/{orderId}/ordersdetail")
	public ResponseEntity<List<OrderDetail>> getOrderDetailByOrderId(@PathVariable("orderId") int orderId) {
		try {
			List<OrderDetail> vOrderDetail = orderDetailRepository.getOrderDetailByOrderId(orderId);
			if (vOrderDetail != null) {
				return new ResponseEntity<>(vOrderDetail, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
//	@PostMapping("/OrderDetail/{orderId}/add")
//	public ResponseEntity<OrderDetail> addOrderDetail(@RequestBody OrderDetail orderDetail){
//		try {
//			OrderDetail newOrderDetail = new OrderDetail();
//			newOrderDetail.setQuantityOrder(orderDetail.getQuantityOrder());
//			newOrderDetail.setBook(orderDetail.getBook());
//			
//			Book savedOrderDetail = orderDetail.save(newOrderDetail);
//			return new ResponseEntity<>(savedOrderDetail, HttpStatus.CREATED);
//		} catch (Exception e) {
//			e.printStackTrace();
//			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
//		}
//	}
//	
//	@PutMapping("/books/update/{id}")
//	public ResponseEntity<Object> updateBook(@PathVariable("id") long id, @RequestBody Book book){
//		Optional<Book> bookData = bookRepository.findById(id);
//		if(bookData.isPresent()) {
//			Book newBook = bookData.get();
//			newBook.setBookCode(book.getBookCode());
//			newBook.setBookTitle(book.getBookTitle());
//			newBook.setBookPublisher(book.getBookPublisher());
//			newBook.setBookDecription(book.getBookDecription());
//			newBook.setAuthor(book.getAuthor());
//			newBook.setLanguage(book.getLanguage());
//			newBook.setQuantityInStock(book.getQuantityInStock());
//			newBook.setBookPrice(book.getBookPrice());
//			newBook.setBookImage(book.getBookImage());
//			newBook.setShortDescription(book.getShortDescription());
//			newBook.setBookDetailsImage(book.getBookDetailsImage());
//			newBook.setBookType(book.getBookType());
//			
//			Book savedBook = bookRepository.save(newBook);
//				
//			return new ResponseEntity<>(savedBook, HttpStatus.OK);
//		} else {
//			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//		}
//	}
	
}
