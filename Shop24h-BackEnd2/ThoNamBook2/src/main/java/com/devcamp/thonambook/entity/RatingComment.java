package com.devcamp.thonambook.entity;

import java.util.Date;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="rating_comment")
public class RatingComment {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private int rating;
	
	private Date dateCreated;
	
	@Lob
	private String comment;
	
	@ManyToOne
	private Users users;
	
	@ManyToOne
	private Book book;
	
	public RatingComment() {
		super();
	}

	

	public RatingComment(long id, int rating, Date dateCreated, String comment, Users users, Book book) {
		super();
		this.id = id;
		this.rating = rating;
		this.dateCreated = dateCreated;
		this.comment = comment;
		this.users = users;
		this.book = book;
	}



	public Date getDateCreated() {
		return dateCreated;
	}



	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}



	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Users getUsers() {
		return users;
	}

	public void setUsers(Users users) {
		this.users = users;
	}

	
	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}
	
	
}
