package com.devcamp.thonambook.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcamp.thonambook.entity.Users;

@Repository
public interface UsersRepository extends JpaRepository<Users, Long>{
	@Query(value = "SELECT * FROM users", nativeQuery = true)
	List<Users> getAllUsers();
	
	@Query(value = "SELECT * FROM users  WHERE phone_number like :phoneNumber", nativeQuery = true)
	Users findUsersByPhoneNumber(@Param("phoneNumber") String phoneNumber);
	
	@Query(value = "SELECT * FROM users  WHERE id like :id", nativeQuery = true)
	Users getUsersInfoById(@Param("id") int id);
	
	@Query(value = "SELECT * from users where users.payment >= :min AND users.payment < :max", nativeQuery = true)
	List<Users> getCustomerByPaymentRange(@Param("min") int min, @Param("max") int max);
	
	@Query(value = "SELECT c.id as id, c.full_name as fullname, c.phone_number as phonenumber, count(c.id) as ordercount FROM `users` c JOIN orders o ON c.id = o.users_id GROUP BY id, fullname, phonenumber HAVING ordercount >= :min AND ordercount < :max", nativeQuery = true)
	List<ICustomerOrderCount> getCustomerOrderCountByRange(@Param("min") int min, @Param("max") int max);
	
	@Query(value = "SELECT * FROM users  WHERE user_name = :userName", nativeQuery = true)
	Users findUsersByUserName(@Param("userName") String userName);
	
	@Query(value = "SELECT * FROM users  WHERE user_name = :userName AND password = :password", nativeQuery = true)
	Users findUsersByUserNameAndPassword(@Param("userName") String userName,@Param("password") String password);
	
	//SELECT count(*) FROM `order_details` d JOIN orders o ON d.order_id=o.id WHERE book_id = 1 and users_id = 2
	@Query(value = "SELECT count(*) FROM `order_details` d JOIN orders o ON d.order_id=o.id WHERE book_id = :bookid and users_id = :userid", nativeQuery = true)
	Integer userOrderBookCount(@Param("bookid") long bookId ,@Param("userid") long userId);
}
