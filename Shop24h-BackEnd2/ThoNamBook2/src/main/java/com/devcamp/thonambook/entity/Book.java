package com.devcamp.thonambook.entity;

import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="books")
public class Book {
	
	public Book() {
		
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name="book_code")
	private String bookCode;
	
	@Column(name="book_title")
	private String bookTitle;
	
	@Column(name="book_publisher")
	private String bookPublisher;
	
	@Lob
	@Column(name="book_decription")
	private String bookDecription;
	
	@Column(name="author")
	private String author;
	
	@Column(name="language")
	private String language;
	
	
	@Column(name="quantity_in_stock")
	private int quantityInStock;
	
	@Column(name="book_price")
	private int bookPrice;
	
	@Column(name="book_image")
	private String bookImage;
	
	@Lob
	@Column(name="short_description")
	private String shortDescription;
	
	@Column(name="book_details_image")
	private String bookDetailsImage;
	
	@Column(name="book_type")
	private String bookType;
	
	@JsonIgnore
	@OneToMany(mappedBy="book")
	private List<OrderDetail> orderDetail;
	
	@JsonIgnore
	@OneToMany(mappedBy="book", cascade = CascadeType.ALL)
	private List<RatingComment> ratingComment;
	
	public Book(long id, String bookCode, String bookTitle, String bookPublisher, String bookDecription, String author,
			String language, int quantityInStock, int bookPrice, String bookImage, String shortDescription,
			String bookDetailsImage, String bookType, List<OrderDetail> orderDetail) {
		super();
		this.id = id;
		this.bookCode = bookCode;
		this.bookTitle = bookTitle;
		this.bookPublisher = bookPublisher;
		this.bookDecription = bookDecription;
		this.author = author;
		this.language = language;
		this.quantityInStock = quantityInStock;
		this.bookPrice = bookPrice;
		this.bookImage = bookImage;
		this.shortDescription = shortDescription;
		this.bookDetailsImage = bookDetailsImage;
		this.bookType = bookType;
		this.orderDetail = orderDetail;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getBookCode() {
		return bookCode;
	}

	public void setBookCode(String bookCode) {
		this.bookCode = bookCode;
	}

	public String getBookTitle() {
		return bookTitle;
	}

	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}

	public String getBookPublisher() {
		return bookPublisher;
	}

	public void setBookPublisher(String bookPublisher) {
		this.bookPublisher = bookPublisher;
	}

	public String getBookDecription() {
		return bookDecription;
	}

	public void setBookDecription(String bookDecription) {
		this.bookDecription = bookDecription;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public int getQuantityInStock() {
		return quantityInStock;
	}

	public void setQuantityInStock(int quantityInStock) {
		this.quantityInStock = quantityInStock;
	}

	public int getBookPrice() {
		return bookPrice;
	}

	public void setBookPrice(int bookPrice) {
		this.bookPrice = bookPrice;
	}

	public String getBookImage() {
		return bookImage;
	}

	public void setBookImage(String bookImage) {
		this.bookImage = bookImage;
	}
	
	@JsonIgnore
	public List<OrderDetail> getOrderDetail() {
		return orderDetail;
	}

	public void setOrderDetail(List<OrderDetail> orderDetail) {
		this.orderDetail = orderDetail;
	}

	public String getShortDecription() {
		return shortDescription;
	}

	public void setShortDecription(String shortDecription) {
		this.shortDescription = shortDecription;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String getBookDetailsImage() {
		return bookDetailsImage;
	}

	public void setBookDetailsImage(String bookDetailsImage) {
		this.bookDetailsImage = bookDetailsImage;
	}

	public String getBookType() {
		return bookType;
	}

	public void setBookType(String bookType) {
		this.bookType = bookType;
	}

	@JsonIgnore
	public List<RatingComment> getRatingComment() {
		return ratingComment;
	}

	public void setRatingComment(List<RatingComment> ratingComment) {
		this.ratingComment = ratingComment;
	}
}
