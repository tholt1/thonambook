package com.devcamp.thonambook.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.thonambook.entity.Book;
import com.devcamp.thonambook.entity.Order;
import com.devcamp.thonambook.entity.OrderDetail;
import com.devcamp.thonambook.repository.OrderRepository;
import com.devcamp.thonambook.service.OrderExcelExporter;

@CrossOrigin
@RestController
public class OrdersController {

	@Autowired
	private OrderRepository orderRepository;
	
	@GetMapping("/orders/all")
	public ResponseEntity<List<Order>> getAllOrders() {
		try {
			List<Order> vOrder = orderRepository.getAllOrders();
			if (vOrder != null) {
				return new ResponseEntity<>(vOrder, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/orders/find/{id}")
	public ResponseEntity<Order> getOrderById(@PathVariable("id") long id) {
		try {
			Optional<Order> vOrder = orderRepository.findById(id);
			if (vOrder.isPresent()) {
				return new ResponseEntity<>(vOrder.get(), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping("/orders/modify/{id}")
	public ResponseEntity<Order> modifyOrder(@RequestBody Order order){
		try {
			Optional<Order> newOrder = orderRepository.findById(order.getId());
			if (newOrder.isPresent()) {
				Order mOrder = newOrder.get();
				mOrder.setStatus(order.getStatus());
				mOrder.setOrderDetails(order.getOrderDetails());
				if (mOrder.getOrderDetails().size() > 0) {
					mOrder.getOrderDetails().forEach(od -> od.setOrder(mOrder));
				}
				
				mOrder.setPayment(order.getPayment());
				
				Order savedOrderDetail = orderRepository.save(mOrder);
				return new ResponseEntity<>(savedOrderDetail, HttpStatus.CREATED);
			}
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/users/{usersId}/orders")
	public ResponseEntity<List<Order>> getOrderInfoByUsersId(@PathVariable("usersId") int usersId) {
		try {
			List<Order> vOrder = orderRepository.getOrderByUsersId(usersId);
			if (vOrder != null) {
				return new ResponseEntity<>(vOrder, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/report/month/{month}")
	public ResponseEntity<Object> getReportByMonth(@PathVariable("month") int month) {
		try {
			var vPayment = orderRepository.getPaymentInMonth(month);
			if (vPayment != null) {
				return new ResponseEntity<>(vPayment, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/report/year/{year}")
	public ResponseEntity<Object> getReportByYear(@PathVariable("year") int year) {
		try {
			var vPayment = orderRepository.getPaymentInYear(year);
			if (vPayment != null) {
				return new ResponseEntity<>(vPayment, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/export/order")
	public void exportToExcel(HttpServletResponse response, @RequestParam String from, @RequestParam String to) {
		try {
			response.setContentType("application/octet-stream");
			//DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
			//String currentDateTime = dateFormatter.format(from);

			String headerKey = "Content-Disposition";
			String headerValue = "attachment; filename=order.xlsx";
			response.setHeader(headerKey, headerValue);
			
			Date fromDate = new SimpleDateFormat("yyyy-MM-dd").parse(from);
			Date toDate = new SimpleDateFormat("yyyy-MM-dd").parse(to);
			List<Order> vOrder = orderRepository.getOrderByDate(fromDate, toDate);

			OrderExcelExporter excelExporter = new OrderExcelExporter(vOrder);

			excelExporter.export(response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
