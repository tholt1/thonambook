package com.devcamp.thonambook.entity;

public class PageResponse {
    private int currentPage;
    private int pageCount;
    private int itemPerPage;
    private int totalItem;
    private Object data;

	public PageResponse(int currentPage, int pageCount, int itemPerPage, int totalItem, Object data) {
		super();
		this.currentPage = currentPage;
		this.pageCount = pageCount;
		this.itemPerPage = itemPerPage;
		this.totalItem = totalItem;
		this.data = data;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getPageCount() {
		return pageCount;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	public int getTotalItem() {
		return totalItem;
	}

	public void setTotalItem(int totalItem) {
		this.totalItem = totalItem;
	}

	public int getItemPerPage() {
		return itemPerPage;
	}

	public void setItemPerPage(int itemPerPage) {
		this.itemPerPage = itemPerPage;
	}
}
