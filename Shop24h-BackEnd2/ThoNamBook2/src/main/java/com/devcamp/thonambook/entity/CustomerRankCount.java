package com.devcamp.thonambook.entity;

public class CustomerRankCount {
	private String name;
	private int count;
	
	public CustomerRankCount(String name, int count) {
		super();
		this.name = name;
		this.count = count;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
	
}
