package com.devcamp.thonambook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThoNamBook2Application {

	public static void main(String[] args) {
		SpringApplication.run(ThoNamBook2Application.class, args);
	}

}
