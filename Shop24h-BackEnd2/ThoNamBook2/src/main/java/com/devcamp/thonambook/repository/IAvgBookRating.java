package com.devcamp.thonambook.repository;

public interface IAvgBookRating {
	public float getAvgRating();
	public int getUserCount();
	public long getBookId();
}
