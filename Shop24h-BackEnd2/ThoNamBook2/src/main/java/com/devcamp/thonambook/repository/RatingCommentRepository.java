package com.devcamp.thonambook.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcamp.thonambook.entity.RatingComment;

@Repository
public interface RatingCommentRepository extends JpaRepository<RatingComment, Long> {
	@Query(value = "SELECT COUNT(*) as UserCount, AVG(`rating`) as AvgRating, book_id  as  BookId FROM `rating_comment` WHERE `book_id` = :id", nativeQuery = true)
	IAvgBookRating getBookAvgRating(@Param("id") long id);
	
	@Query(value = "select * from rating_comment where book_id = :id", nativeQuery = true)
	Page<RatingComment> getRatingComment(Pageable page,@Param("id") long id);
	
	@Query(value = "SELECT u.full_name as UserName,u.id as userId, r.rating as rating, r.comment as comment, r.date_created as CreatedDate FROM rating_comment r JOIN users u ON r.users_id=u.id where r.book_id=:id",
			countQuery = "select count(*) FROM rating_comment r JOIN users u ON r.users_id=u.id where r.book_id=:id", nativeQuery = true)
	Page<IBookRatingComment> getBookRatingComment(Pageable page,@Param("id") long id);
	
	//select count(*) from rating_comment where users_id = 3
	@Query(value = "select count(*) from rating_comment where users_id=:uid and book_id=:bid", nativeQuery = true)
	Integer getUserBookCommentCount(@Param("uid") long userId, @Param("bid") long bookId);
	
	@Query(value = "SELECT * FROM rating_comment", nativeQuery = true)
	List<RatingComment> getAllRatingComment();
}
