package com.devcamp.thonambook.repository;

import java.util.List;

import com.devcamp.thonambook.entity.Voucher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface VoucherRepository extends JpaRepository<Voucher, Long> {
	@Query(value = "SELECT * FROM vouchers", nativeQuery = true)
	List<Voucher> getAllVoucher();
	
	@Query(value = "SELECT * FROM vouchers  WHERE ma_voucher like :maVoucher", nativeQuery = true)
	Voucher findVoucherByMaVoucher(@Param("maVoucher") String maVoucher);
}
