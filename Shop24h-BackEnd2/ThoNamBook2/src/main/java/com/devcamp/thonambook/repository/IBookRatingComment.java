package com.devcamp.thonambook.repository;

import java.util.Date;

public interface IBookRatingComment {
	public String getUserName();
	public int getRating();
	public String getComment();
	public Date getCreatedDate();
	public long getUserId();
}
