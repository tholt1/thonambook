package com.devcamp.thonambook.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "vouchers")
public class Voucher {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotNull(message = "Phải có mã giảm giá")
    @Size(min = 6, message = "Mã voucher phải có ít nhất 6 ký tự ")
    @Column(name = "ma_voucher", unique = true)
    private String maVoucher;

    @NotEmpty(message = "Nhập giá trị giảm giá")
    @Column(name = "phan_tram_giam_gia")
    private int phanTramGiamGia;

    @Column(name = "ghi_chu")
    private String ghiChu;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ngay_tao", nullable = true, updatable = false)
    @CreatedDate
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date ngayTao;
    
    @JsonIgnore
    @OneToMany(mappedBy="voucher")
    private List<Order> order;
    
    public Voucher() {
    };
    
	public Voucher(long id,
			@NotNull(message = "Phải có mã giảm giá") @Size(min = 6, message = "Mã voucher phải có ít nhất 6 ký tự ") String maVoucher,
			@NotEmpty(message = "Nhập giá trị giảm giá") int phanTramGiamGia, String ghiChu, Date ngayTao,
			List<Order> order) {
		super();
		this.id = id;
		this.maVoucher = maVoucher;
		this.phanTramGiamGia = phanTramGiamGia;
		this.ghiChu = ghiChu;
		this.ngayTao = ngayTao;
		this.order = order;
	}

	public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMaVoucher() {
        return maVoucher;
    }

    public void setMaVoucher(String maVoucher) {
        this.maVoucher = maVoucher;
    }

    public int getPhanTramGiamGia() {
        return phanTramGiamGia;
    }

    public void setPhanTramGiamGia(int phanTramGiamGia) {
        this.phanTramGiamGia = phanTramGiamGia;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public Date getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(Date ngayTao) {
        this.ngayTao = ngayTao;
    }

	public List<Order> getOrder() {
		return order;
	}

	public void setOrder(List<Order> order) {
		this.order = order;
	}

}