package com.devcamp.thonambook.repository;

public interface ICustomerOrderCount {
	public long getId();
	public String getFullName();
	public String getPhoneNumber();
	public int getOrderCount();
}
