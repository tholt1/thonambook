package com.devcamp.thonambook.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcamp.thonambook.entity.Book;

@Repository
public interface BookRepository extends JpaRepository<Book, Long>{
	
	@Query(value = "SELECT * FROM books ORDER BY rand() LIMIT 3", nativeQuery = true)
	List<Book> findBookByRandom();
	
	@Query(value = "SELECT * FROM books ORDER BY id DESC LIMIT 8", nativeQuery = true)
	List<Book> findBookOrderById();
	
	@Query(value = "SELECT * FROM books ORDER BY rand() LIMIT 6", nativeQuery = true)
	List<Book> findBookByRandomSixSample();
	
	@Query(value = "SELECT * FROM books", nativeQuery = true)
	List<Book> getAllBookByPage(Pageable page);
	
	@Query(value = "SELECT * FROM books", nativeQuery = true)
	List<Book> getAllBook();
	
	@Query(value = "select * from books where (:category is null or :category = book_type) AND (:language is null or :language = language) AND (:pmin is null or :pmin <= book_price) AND (:pmax is null or :pmax >= book_price) AND (:publisher is null or :publisher = book_publisher)", nativeQuery = true)
	Page<Book> filterBook(Pageable page, @Param("category") String category, @Param("pmin") int priceMin, @Param("pmax") int priceMax, @Param("publisher") String publisher, @Param("language") String language);
	
	@Query(value = "SELECT * FROM books  WHERE id like :id", nativeQuery = true)
	Book getBookInfoById(@Param("id") int id);
	
	@Query(value = "SELECT * FROM books WHERE book_type = :type", nativeQuery = true)
	List<Book> findBookByType(@Param("type") String type);
	

}
