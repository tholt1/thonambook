package com.devcamp.thonambook.entity;

import javax.persistence.*;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.util.Date;
import java.util.List;


@Entity
@Table(name="orders")
public class Order  {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private String comments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "order_date", nullable = false, updatable = false)
    @CreatedDate
	private Date orderDate;

	private String status;
	
	private int payment;
	
	@OneToMany(mappedBy="order", cascade = CascadeType.ALL)
	private List<OrderDetail> orderDetails;

	@JsonIgnore
	@ManyToOne
	private Users users;
	
	@ManyToOne(optional=true)
	private Voucher voucher;

	public Order() {
		
	}

	public Order(long id, String comments, Date orderDate, String status, int payment, List<OrderDetail> orderDetails,
			Users users, Voucher voucher) {
		super();
		this.id = id;
		this.comments = comments;
		this.orderDate = orderDate;
		this.status = status;
		this.payment = payment;
		this.orderDetails = orderDetails;
		this.users = users;
		this.voucher = voucher;
	}



	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getOrderDate() {
		return this.orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}


	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<OrderDetail> getOrderDetails() {
		return this.orderDetails;
	}

	public void setOrderDetails(List<OrderDetail> orderDetails) {
		this.orderDetails = orderDetails;
	}

	public OrderDetail addOrderDetail(OrderDetail orderDetail) {
		getOrderDetails().add(orderDetail);
		orderDetail.setOrder(this);

		return orderDetail;
	}

	public OrderDetail removeOrderDetail(OrderDetail orderDetail) {
		getOrderDetails().remove(orderDetail);
		orderDetail.setOrder(null);

		return orderDetail;
	}

	@JsonIgnore
	public Users getUsers() {
		return this.users;
	}

	public void setUsers(Users users) {
		this.users = users;
	}

	public int getPayment() {
		return payment;
	}

	public void setPayment(int payment) {
		this.payment = payment;
	}

	public Voucher getVoucher() {
		return voucher;
	}

	public void setVoucher(Voucher voucher) {
		this.voucher = voucher;
	}

}