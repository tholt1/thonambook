package com.devcamp.thonambook.repository;

public interface IMonthlyPayment {
	public int getMonth();
	public int getYear();
	public int getPayment();
}
