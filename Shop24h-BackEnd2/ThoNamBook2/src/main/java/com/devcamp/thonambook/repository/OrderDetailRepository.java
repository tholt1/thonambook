package com.devcamp.thonambook.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcamp.thonambook.entity.OrderDetail;

@Repository
public interface OrderDetailRepository extends JpaRepository<OrderDetail, Long>{
	@Query(value = "SELECT * FROM order_details  WHERE order_id like :orderId", nativeQuery = true)
	List<OrderDetail> getOrderDetailByOrderId(@Param("orderId") int orderId);
}
