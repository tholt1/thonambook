package com.devcamp.thonambook.entity;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.util.List;

@Entity
@Table(name="users")
public class Users  {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private String address;

	@Column(name="city_state")
	private String cityState;

	private String region;

	@Column(name="full_name")
	private String fullName;

	@Column(name="phone_number", unique = true)
	private String phoneNumber;

	private int payment;
	
	private String email;
	
	@Column(name="user_name")
	private String userName;
	
	private String password;
	
	private int role;
	
	private int register;
	
	@OneToMany(mappedBy="users", cascade = CascadeType.ALL)
	private List<Order> orders;

	@JsonIgnore
	@OneToMany(mappedBy="users", cascade = CascadeType.ALL)
	private List<RatingComment> ratingComment;
	
	public Users() {
		
	}

	public Users(long id, String address, String cityState, String region, String fullName, String phoneNumber,
			int payment, String email, String userName, String password, int role, int register, List<Order> orders,
			List<RatingComment> ratingComment) {
		super();
		this.id = id;
		this.address = address;
		this.cityState = cityState;
		this.region = region;
		this.fullName = fullName;
		this.phoneNumber = phoneNumber;
		this.payment = payment;
		this.email = email;
		this.userName = userName;
		this.password = password;
		this.role = role;
		this.register = register;
		this.orders = orders;
		this.ratingComment = ratingComment;
	}

	public long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	
	public String getCityState() {
		return cityState;
	}


	public void setCityState(String cityState) {
		this.cityState = cityState;
	}

	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}


	public String getFullName() {
		return fullName;
	}



	public void setFullName(String fullName) {
		this.fullName = fullName;
	}



	public int getPayment() {
		return payment;
	}



	public void setPayment(int payment) {
		this.payment = payment;
	}

	public List<Order> getOrders() {
		return this.orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}

	public Order addOrder(Order order) {
		getOrders().add(order);
		order.setUsers(this);

		return order;
	}

	public Order removeOrder(Order order) {
		getOrders().remove(order);
		order.setUsers(null);

		return order;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public String getUserName() {
		return userName;
	}



	public void setUserName(String userName) {
		this.userName = userName;
	}



	public String getPassword() {
		return password;
	}



	public void setPassword(String password) {
		this.password = password;
	}



	public int getRole() {
		return role;
	}



	public void setRole(int role) {
		this.role = role;
	}



	public int getRegister() {
		return register;
	}



	public void setRegister(int register) {
		this.register = register;
	}

	public void setId(long id) {
		this.id = id;
	}

	@JsonIgnore
	public List<RatingComment> getRatingComment() {
		return ratingComment;
	}

	public void setRatingComment(List<RatingComment> ratingComment) {
		this.ratingComment = ratingComment;
	}
	
}