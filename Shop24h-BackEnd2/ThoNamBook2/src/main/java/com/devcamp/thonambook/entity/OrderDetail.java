package com.devcamp.thonambook.entity;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="order_details")
public class OrderDetail  {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name="quantity_order")
	private int quantityOrder;

	//@JsonIgnore
	@ManyToOne
	private Book book;

	
	//@JsonIgnore
	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	
	
	public OrderDetail(long id, int quantityOrder, Book book, Order order) {
		super();
		this.id = id;
		this.quantityOrder = quantityOrder;
		this.book = book;
		this.order = order;
	}



	@JsonIgnore
	@ManyToOne
	private Order order;

	public OrderDetail() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getQuantityOrder() {
		return this.quantityOrder;
	}

	public void setQuantityOrder(int quantityOrder) {
		this.quantityOrder = quantityOrder;
	}

	@JsonIgnore
	public Order getOrder() {
		return this.order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

}