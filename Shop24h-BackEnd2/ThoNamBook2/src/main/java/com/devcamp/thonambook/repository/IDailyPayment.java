package com.devcamp.thonambook.repository;

import java.util.Date;

public interface IDailyPayment {
	public Date getDate();
	public int getPayment();
}
