package com.devcamp.thonambook.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcamp.thonambook.entity.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long>{
	@Query(value = "SELECT * FROM orders  WHERE users_id like :usersId", nativeQuery = true)
	List<Order> getOrderByUsersId(@Param("usersId") int id);
	
	@Query(value = "SELECT * FROM orders", nativeQuery = true)
	List<Order> getAllOrders();
	
//	@Query(value = "SELECT * FROM orders where ", nativeQuery = true)
//	List<Order> findOrderById();
	
	@Query(value = "SELECT DATE(order_date) as date,sum(payment) as payment FROM `orders` GROUP BY date HAVING MONTH(date)=:month", nativeQuery = true)
	List<IDailyPayment> getPaymentInMonth(@Param("month") int month);
	
	@Query(value = "SELECT MONTH(order_date) as month, YEAR(order_date) as year,sum(payment)as payment FROM `orders` GROUP BY month, year HAVING year=:year", nativeQuery = true)
	List<IMonthlyPayment> getPaymentInYear(@Param("year") int year);
	
	@Query(value = "SELECT * FROM `orders` WHERE order_date >= :from AND order_date <= :to ", nativeQuery = true)
	List<Order> getOrderByDate(@Param("from") Date from, @Param("to") Date to);
	
}
