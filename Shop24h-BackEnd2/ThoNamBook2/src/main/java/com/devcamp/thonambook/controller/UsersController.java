package com.devcamp.thonambook.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.thonambook.entity.Users;
import com.devcamp.thonambook.entity.CustomerOrderCountReport;
import com.devcamp.thonambook.entity.CustomerRankCount;
import com.devcamp.thonambook.entity.Order;
import com.devcamp.thonambook.repository.UsersRepository;
import com.devcamp.thonambook.service.CustomerExcelExporter;
import com.devcamp.thonambook.service.OrderExcelExporter;

@CrossOrigin
@RestController
public class UsersController {

	@Autowired
	private UsersRepository usersRepository;

	@GetMapping("/users/all")
	public ResponseEntity<List<Users>> getAllUsers() {
		try {
			List<Users> vUser = usersRepository.getAllUsers();
			if (vUser != null) {
				return new ResponseEntity<>(vUser, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/users/findphone/{phonenumber}")
	public ResponseEntity<Users> getUsersByPhoneNumber(@PathVariable("phonenumber") String phoneNumber) {
		try {
			Users vUsers = usersRepository.findUsersByPhoneNumber(phoneNumber);
			if (vUsers != null) {
				return new ResponseEntity<>(vUsers, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/users/findname/{username}")
	public ResponseEntity<Object> getUsersByUserName(@PathVariable("username") String userName) {
		try {
			Users vUsers = usersRepository.findUsersByUserName(userName);
			if (vUsers != null) {
				return new ResponseEntity<>(true, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(false, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/users/findusernamepassword/{username}/{password}")
	public ResponseEntity<Users> getUsersByUserNameAndPassword(@PathVariable("username") String userName,@PathVariable("password") String password) {
		try {
			Users vUsers = usersRepository.findUsersByUserNameAndPassword(userName,password);
			if (vUsers != null) {
				return new ResponseEntity<>(vUsers, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/users/{id}")
	public ResponseEntity<Users> getUsersInfoById(@PathVariable("id") int id) {
		try {
			Users vUsers = usersRepository.getUsersInfoById(id);
			if (vUsers != null) {
				return new ResponseEntity<>(vUsers, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("/users/add")
	public ResponseEntity<Users> addUsers(@RequestBody Users users) {
		try {
			Users newUsers = new Users();
			newUsers.setFullName(users.getFullName());
			newUsers.setPhoneNumber(users.getPhoneNumber());
			newUsers.setAddress(users.getAddress());
			newUsers.setCityState(users.getCityState());
			newUsers.setRegion(users.getRegion());
			newUsers.setPayment(users.getPayment());
			newUsers.setEmail(users.getEmail());
			newUsers.setOrders(users.getOrders());
			newUsers.setUserName(users.getUserName());
			newUsers.setPassword(users.getPassword());
			newUsers.setRole(users.getRole());
			newUsers.setRegister(users.getRegister());
			
			if (newUsers.getOrders().size() > 0) {
				newUsers.getOrders().get(newUsers.getOrders().size() - 1).setOrderDate(new Date());
				newUsers.getOrders().forEach(order -> order.setUsers(newUsers));
				newUsers.getOrders().forEach(order -> order.getOrderDetails().forEach(od -> od.setOrder(order)));
			}
			Users savedUsers = usersRepository.save(newUsers);
			return new ResponseEntity<>(savedUsers, HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/users/update/{id}")
	public ResponseEntity<Object> updateUsers(@PathVariable("id") long id, @RequestBody Users users) {
		Optional<Users> usersData = usersRepository.findById(id);
		if (usersData.isPresent()) {
			Users newUsers = usersData.get();
			newUsers.setFullName(users.getFullName());
			newUsers.setPhoneNumber(users.getPhoneNumber());
			newUsers.setAddress(users.getAddress());
			newUsers.setCityState(users.getCityState());
			newUsers.setRegion(users.getRegion());
			newUsers.setPayment(users.getPayment());
			newUsers.setEmail(users.getEmail());
			newUsers.setOrders(users.getOrders());
			newUsers.setUserName(users.getUserName());
			newUsers.setPassword(users.getPassword());
			newUsers.setRole(users.getRole());
			newUsers.setRegister(users.getRegister());
			
			if (newUsers.getOrders().size() > 0) {
				newUsers.getOrders().get(newUsers.getOrders().size() - 1).setOrderDate(new Date());
				newUsers.getOrders().forEach(order -> order.setUsers(newUsers));
				newUsers.getOrders().forEach(order -> order.getOrderDetails().forEach(od -> od.setOrder(order)));
			}

			Users savedUsers = usersRepository.save(newUsers);

//			for (Order order : customer.getOrders()) {
//				order.setCustomer(savedCustomer);
//				
//				orderRepository.save(order);
//				
//			}

			return new ResponseEntity<>(savedUsers, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping("/customer/report/rank")
	public ResponseEntity<List<CustomerRankCount>> getCustomerCountByRank() {
		try {
			List<Users> lVip = usersRepository.getCustomerByPaymentRange(5000000, 10000000);
			List<Users> lSilver = usersRepository.getCustomerByPaymentRange(10000000, 20000000);
			List<Users> lGold = usersRepository.getCustomerByPaymentRange(20000000, 50000000);
			List<Users> lPlatinum = usersRepository.getCustomerByPaymentRange(50000000, Integer.MAX_VALUE);
			List<CustomerRankCount> list = new ArrayList<>();
			list.add(new CustomerRankCount("VIP", lVip.size()));
			list.add(new CustomerRankCount("Bạc", lSilver.size()));
			list.add(new CustomerRankCount("Vàng", lGold.size()));
			list.add(new CustomerRankCount("Bạch kim", lPlatinum.size()));
			
			return new ResponseEntity<>(list, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/export/customer")
	public void exportToExcel(HttpServletResponse response) {
		try {
			response.setContentType("application/octet-stream");
			//DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
			//String currentDateTime = dateFormatter.format(from);

			String headerKey = "Content-Disposition";
			String headerValue = "attachment; filename=customer.xlsx";
			response.setHeader(headerKey, headerValue);
			
			List<Users> lVip = usersRepository.getCustomerByPaymentRange(5000000, 10000000);
			List<Users> lSilver = usersRepository.getCustomerByPaymentRange(10000000, 20000000);
			List<Users> lGold = usersRepository.getCustomerByPaymentRange(20000000, 50000000);
			List<Users> lPlatinum = usersRepository.getCustomerByPaymentRange(50000000, Integer.MAX_VALUE);

			CustomerExcelExporter excelExporter = new CustomerExcelExporter(lVip, lSilver, lGold, lPlatinum);

			excelExporter.export(response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@GetMapping("/customer/report/order")
	public ResponseEntity<Object> getCustomerCountByOrderRange() {
		try {
			var list0010 = usersRepository.getCustomerOrderCountByRange(1, 10);
			var list1050 = usersRepository.getCustomerOrderCountByRange(10, 50);
			var list50100 = usersRepository.getCustomerOrderCountByRange(50, 100);
			var list100 = usersRepository.getCustomerOrderCountByRange(100, 9999999);
			List<CustomerOrderCountReport> list = new ArrayList<>();
			list.add(new CustomerOrderCountReport("1-9", list0010.size()));
			list.add(new CustomerOrderCountReport("10-49", list1050.size()));
			list.add(new CustomerOrderCountReport("50-99", list50100.size()));
			list.add(new CustomerOrderCountReport("Trên 100", list100.size()));
			
			return new ResponseEntity<>(list, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/user/checkbook/{uid}/{bid}")
	public ResponseEntity<Object> checkUserOrderBook(@PathVariable("uid") long userId, @PathVariable("bid") long bookId) {
		try {
			Integer count = usersRepository.userOrderBookCount(bookId, userId);
			
			return new ResponseEntity<>(count > 0, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
