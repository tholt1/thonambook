package com.devcamp.thonambook.service;

import java.util.List;
import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.devcamp.thonambook.entity.Order;

public class OrderExcelExporter {
	private XSSFWorkbook workbook;
	private XSSFSheet sheet;
	private List<Order> orders;

	public OrderExcelExporter(List<Order> orders) {
		this.orders = orders;
		workbook = new XSSFWorkbook();
	}

	private void createCell(Row row, int columnIndex, Object value, CellStyle style) {
		sheet.autoSizeColumn(columnIndex);
		Cell cell = row.createCell(columnIndex);
		if (value instanceof Integer) {
			cell.setCellValue((Integer) value);
		} else if (value instanceof Boolean) {
			cell.setCellValue((Boolean) value);
		} else {
			cell.setCellValue((String) value);
		}
		cell.setCellStyle(style);
	}

	private void writeHeaderLine() {
		sheet = workbook.createSheet("Orders");

		Row row = sheet.createRow(0);

		CellStyle style = workbook.createCellStyle();
		XSSFFont font = workbook.createFont();
		font.setBold(true);
		font.setFontHeight(16);
		style.setFont(font);

		createCell(row, 0, "ID", style);
		createCell(row, 1, "Ngày tạo", style);
		createCell(row, 2, "Tên khách hàng", style);
		createCell(row, 3, "Tổng thanh toán", style);
		createCell(row, 4, "Mã giảm giá", style);
	}

	private void writeDataLines() {
		int rowCount = 1;

		CellStyle style = workbook.createCellStyle();
		XSSFFont font = workbook.createFont();
		font.setFontHeight(14);
		style.setFont(font);

		for (Order order : this.orders) {
			Row row = sheet.createRow(rowCount++);
			int columnCount = 0;

			createCell(row, columnCount++, Long.toString(order.getId()), style);
			createCell(row, columnCount++, order.getOrderDate().toString(), style);
			createCell(row, columnCount++, order.getUsers().getFullName(), style);
			createCell(row, columnCount++, order.getPayment(), style);
			createCell(row, columnCount++, order.getVoucher() == null ? "":order.getVoucher().getMaVoucher(), style);
		}
	}

	public void export(HttpServletResponse response) throws IOException {
		writeHeaderLine();
		writeDataLines();

		ServletOutputStream outputStream = response.getOutputStream();
		workbook.write(outputStream);
		workbook.close();

		outputStream.close();

	}
}
