package com.devcamp.thonambook.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.thonambook.entity.Voucher;
import com.devcamp.thonambook.repository.VoucherRepository;

@CrossOrigin
@RestController
public class VoucherController {

	@Autowired
	private VoucherRepository voucherRepository;
	
	@GetMapping("/vouchers/all")
	public ResponseEntity<List<Voucher>> getAllVouchers(){
		try {
			List<Voucher> vVoucher = voucherRepository.getAllVoucher();
			if (vVoucher != null) {
				return new ResponseEntity<>(vVoucher,HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/voucher/find/{mavoucher}")
	public ResponseEntity<Voucher> getVoucherByMaVoucher(@PathVariable("mavoucher")String maVoucher){
		try {
			Voucher vVoucher = voucherRepository.findVoucherByMaVoucher(maVoucher);
			if (vVoucher != null) {
				return new ResponseEntity<>(vVoucher,HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
