package com.devcamp.thonambook.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.thonambook.entity.PageResponse;
import com.devcamp.thonambook.entity.RatingComment;
import com.devcamp.thonambook.entity.Users;
import com.devcamp.thonambook.repository.IBookRatingComment;
import com.devcamp.thonambook.repository.RatingCommentRepository;

@CrossOrigin
@RestController
public class RatingCommentController {

	@Autowired
	private RatingCommentRepository ratingCommentRepository;

	@PostMapping("/ratingcomment/add")
	public ResponseEntity<RatingComment> addRatingComment(@RequestBody RatingComment ratingComment) {
		try {
			RatingComment newRatingComment = new RatingComment();
			newRatingComment.setRating(ratingComment.getRating());
			newRatingComment.setComment(ratingComment.getComment());
			newRatingComment.setUsers(ratingComment.getUsers());
			newRatingComment.setBook(ratingComment.getBook());
			newRatingComment.setDateCreated(new Date());

			RatingComment savedRatingComment = ratingCommentRepository.save(newRatingComment);
			return new ResponseEntity<>(savedRatingComment, HttpStatus.CREATED);
		}

		catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/avgbookrating/{id}")
	public ResponseEntity<Object> getAvgBookRating(@PathVariable("id") long id) {
		try {
			var newAvg = ratingCommentRepository.getBookAvgRating(id);
			if (newAvg != null)
				return new ResponseEntity<>(newAvg, HttpStatus.OK);
			else
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/ratingcomment/{id}")
	public ResponseEntity<PageResponse> getBookRatingComment(@PathVariable("id") long id,
			@RequestParam(required = false, defaultValue = "1") String page) {
		try {
			int p = Integer.parseInt(page) - 1;
			int PER_PAGE = 5;
			if (p < 0)
				p = 0;
			Page<IBookRatingComment> pRatingComment = ratingCommentRepository.getBookRatingComment(PageRequest.of(p, PER_PAGE),
					id);
			List<IBookRatingComment> vRatingComment = pRatingComment.getContent();
			PageResponse pr = new PageResponse(p, pRatingComment.getTotalPages(), vRatingComment.size(),
					(int) pRatingComment.getTotalElements(), vRatingComment);
			if (p > pRatingComment.getTotalPages())
				p = pRatingComment.getTotalPages();

			if (vRatingComment != null) {
				return new ResponseEntity<>(pr, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/ratingcomment/user/{uid}/{bid}")
	public ResponseEntity<Object> checkUserBookComment(@PathVariable("uid") long uid, @PathVariable("bid") long bid) {
		try {
			Integer count = ratingCommentRepository.getUserBookCommentCount(uid, bid);
			return new ResponseEntity<>(count > 0, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/ratingcomment/all")
	public ResponseEntity<List<RatingComment>> getRatingComment() {
		try {
			List<RatingComment> vRatingComment = ratingCommentRepository.getAllRatingComment();
			if (vRatingComment != null) {
				return new ResponseEntity<>(vRatingComment, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@DeleteMapping("/ratingcomment/delete/{id}")
	public ResponseEntity<Object> deleteRatingCommentById(@PathVariable long id) {
		try {
			ratingCommentRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e){
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
