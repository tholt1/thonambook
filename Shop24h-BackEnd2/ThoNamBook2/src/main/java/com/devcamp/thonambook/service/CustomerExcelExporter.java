package com.devcamp.thonambook.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.devcamp.thonambook.entity.Users;
import com.devcamp.thonambook.entity.Order;

public class CustomerExcelExporter {
	private XSSFWorkbook workbook;
	private List<XSSFSheet> sheets;
	private List<Users> listVip;
	private List<Users> listSilver;
	private List<Users> listGold;
	private List<Users> listPlatinum;
	
	public CustomerExcelExporter(List<Users> listVip, List<Users> listSilver, List<Users> listGold,
			List<Users> listPlatinum) {
		super();
		this.listVip = listVip;
		this.listSilver = listSilver;
		this.listGold = listGold;
		this.listPlatinum = listPlatinum;
		
		this.workbook = new XSSFWorkbook();
		
		this.sheets = new ArrayList<>();
		this.sheets.add(workbook.createSheet("VIP"));
		this.sheets.add(workbook.createSheet("Bạc"));
		this.sheets.add(workbook.createSheet("Vàng"));
		this.sheets.add(workbook.createSheet("Bạch kim"));
	}

	private void createCell(XSSFSheet sheet, Row row, int columnIndex, Object value, CellStyle style) {
		sheet.autoSizeColumn(columnIndex);
		Cell cell = row.createCell(columnIndex);
		if (value instanceof Integer) {
			cell.setCellValue((Integer) value);
		} else if (value instanceof Boolean) {
			cell.setCellValue((Boolean) value);
		} else {
			cell.setCellValue((String) value);
		}
		cell.setCellStyle(style);
	}

	private void writeHeaderLine(XSSFSheet sheet) {
		Row row = sheet.createRow(0);

		CellStyle style = workbook.createCellStyle();
		XSSFFont font = workbook.createFont();
		font.setBold(true);
		font.setFontHeight(16);
		style.setFont(font);

		createCell(sheet, row, 0, "ID", style);
		createCell(sheet, row, 1, "Tên khách hàng", style);
		createCell(sheet, row, 2, "Số điện thoại", style);
		createCell(sheet, row, 3, "Email", style);
		createCell(sheet, row, 4, "Tỉnh", style);
		createCell(sheet, row, 5, "Thành phố / Huyện", style);
		createCell(sheet, row, 6, "Địa chỉ", style);
		createCell(sheet, row, 7, "Số tiền thanh toán", style);
	}

	private void writeDataLines(XSSFSheet sheet, List<Users> list) {
		int rowCount = 1;

		CellStyle style = workbook.createCellStyle();
		XSSFFont font = workbook.createFont();
		font.setFontHeight(14);
		style.setFont(font);

		for (Users users : list) {
			Row row = sheet.createRow(rowCount++);
			int columnCount = 0;

			createCell(sheet, row, columnCount++, Long.toString(users.getId()), style);
			createCell(sheet, row, columnCount++, users.getFullName(), style);
			createCell(sheet, row, columnCount++, users.getPhoneNumber(), style);
			createCell(sheet, row, columnCount++, users.getEmail(), style);
			createCell(sheet, row, columnCount++, users.getRegion(), style);
			createCell(sheet, row, columnCount++, users.getCityState(), style);
			createCell(sheet, row, columnCount++, users.getAddress(), style);
			createCell(sheet, row, columnCount++, users.getPayment(), style);
		}
	}

	public void export(HttpServletResponse response) throws IOException {
		writeHeaderLine(this.sheets.get(0));
		writeDataLines(this.sheets.get(0), this.listVip);
		
		writeHeaderLine(this.sheets.get(1));
		writeDataLines(this.sheets.get(1), this.listSilver);
		
		writeHeaderLine(this.sheets.get(2));
		writeDataLines(this.sheets.get(2), this.listGold);
		
		writeHeaderLine(this.sheets.get(3));
		writeDataLines(this.sheets.get(3), this.listPlatinum);

		ServletOutputStream outputStream = response.getOutputStream();
		workbook.write(outputStream);
		workbook.close();

		outputStream.close();

	}
}
