package com.devcamp.thonambook.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.thonambook.entity.Book;
import com.devcamp.thonambook.entity.Users;
import com.devcamp.thonambook.entity.PageResponse;
import com.devcamp.thonambook.repository.BookRepository;

@CrossOrigin
@RestController
public class BookController {

	@Autowired
	private BookRepository bookRepository;
	
	@GetMapping("/books/random")
	public ResponseEntity<List<Book>> getBookByRandom(){
		try {
			var vBook = bookRepository.findBookByRandom();
			if (vBook != null) {
				return new ResponseEntity<>(vBook,HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/books/randomsixsample")
	public ResponseEntity<List<Book>> getBookByRandomSixSample(){
		try {
			var vBook = bookRepository.findBookByRandomSixSample();
			if (vBook != null) {
				return new ResponseEntity<>(vBook,HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/books/order")
	public ResponseEntity<List<Book>> getBookOrderById(){
		try {
			var vBook = bookRepository.findBookOrderById();
			if (vBook != null) {
				return new ResponseEntity<>(vBook,HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/books/page/{page}")
	public ResponseEntity<PageResponse> getAllBookOrder(@PathVariable("page") String page){
		try {
			int p = Integer.parseInt(page);
			if (p < 0) p = 0;
			Page<Book> pBook = bookRepository.findAll(PageRequest.of(p, 12));
			List<Book> vBook = pBook.getContent();
			PageResponse pr = new PageResponse(p, pBook.getTotalPages(), vBook.size(), (int)pBook.getTotalElements(), vBook);
			if (p > pBook.getTotalPages()) p = pBook.getTotalPages();
			//var vBook = bookRepository.findBookOrderById();
			if (vBook != null) {
				return new ResponseEntity<>(pr,HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/vietnamesbooks/page/{page}")
	public ResponseEntity<PageResponse> getAllVietNameseBook(@PathVariable("page") String page){
		try {
			int p = Integer.parseInt(page);
			if (p < 0) p = 0;
			Page<Book> pBook = bookRepository.findAll(PageRequest.of(p, 12));
			List<Book> vBook = pBook.getContent();
			PageResponse pr = new PageResponse(p, pBook.getTotalPages(), vBook.size(), (int)pBook.getTotalElements(), vBook);
			if (p > pBook.getTotalPages()) p = pBook.getTotalPages();
			//var vBook = bookRepository.findBookOrderById();
			if (vBook != null) {
				return new ResponseEntity<>(pr,HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/books/all")
	public ResponseEntity<List<Book>> getAllVietNameseBook(){
		try {
			List<Book> vBook = bookRepository.getAllBook();
			if (vBook != null) {
				return new ResponseEntity<>(vBook,HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/books/filter")
	public ResponseEntity<PageResponse> getFilterBook(@RequestParam(required = false) String category, 
													@RequestParam(required = false, defaultValue = "0") String priceMin, 
													@RequestParam(required = false, defaultValue = "2000000000") String priceMax,
													@RequestParam(required = false) String publisher,
													@RequestParam(required = false) String language,
													@RequestParam(required = false, defaultValue = "1") String page){
		try {
			int p = Integer.parseInt(page) - 1;
			int PER_PAGE = 12;
			if (p < 0) p = 0;
			Page<Book> pBook = bookRepository.filterBook(PageRequest.of(p, PER_PAGE), category, Integer.parseInt(priceMin), Integer.parseInt(priceMax), publisher, language);
			List<Book> vBook = pBook.getContent();
			PageResponse pr = new PageResponse(p, pBook.getTotalPages(), vBook.size(), (int)pBook.getTotalElements(), vBook);
			if (p > pBook.getTotalPages()) p = pBook.getTotalPages();
			//var vBook = bookRepository.findBookOrderById();
			if (vBook != null) {
				return new ResponseEntity<>(pr,HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/books/{id}")
	public ResponseEntity<Book> getBookInfoById(@PathVariable("id") int id){
		try {
			Book vBook = bookRepository.getBookInfoById(id);
			if (vBook != null) {
				return new ResponseEntity<>(vBook,HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/books/type/{type}")
	public ResponseEntity<List<Book>> getBookByType(@PathVariable("type") String type){
		try {
			List<Book> vBook = bookRepository.findBookByType(type);
			if (vBook != null) {
				return new ResponseEntity<>(vBook,HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping("/books/add")
	public ResponseEntity<Book> addBook(@RequestBody Book book){
		try {
			Book newBook = new Book();
			newBook.setBookCode(book.getBookCode());
			newBook.setBookTitle(book.getBookTitle());
			newBook.setBookPublisher(book.getBookPublisher());
			newBook.setBookDecription(book.getBookDecription());
			newBook.setAuthor(book.getAuthor());
			newBook.setLanguage(book.getLanguage());
			newBook.setQuantityInStock(book.getQuantityInStock());
			newBook.setBookPrice(book.getBookPrice());
			newBook.setBookImage(book.getBookImage());
			newBook.setShortDescription(book.getShortDescription());
			newBook.setBookDetailsImage(book.getBookDetailsImage());
			newBook.setBookType(book.getBookType());
			
			Book savedBook = bookRepository.save(newBook);
			return new ResponseEntity<>(savedBook, HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping("/books/update/{id}")
	public ResponseEntity<Object> updateBook(@PathVariable("id") long id, @RequestBody Book book){
		Optional<Book> bookData = bookRepository.findById(id);
		if(bookData.isPresent()) {
			Book newBook = bookData.get();
			newBook.setBookCode(book.getBookCode());
			newBook.setBookTitle(book.getBookTitle());
			newBook.setBookPublisher(book.getBookPublisher());
			newBook.setBookDecription(book.getBookDecription());
			newBook.setAuthor(book.getAuthor());
			newBook.setLanguage(book.getLanguage());
			newBook.setQuantityInStock(book.getQuantityInStock());
			newBook.setBookPrice(book.getBookPrice());
			newBook.setBookImage(book.getBookImage());
			newBook.setShortDescription(book.getShortDescription());
			newBook.setBookDetailsImage(book.getBookDetailsImage());
			newBook.setBookType(book.getBookType());
			
			Book savedBook = bookRepository.save(newBook);
				
			return new ResponseEntity<>(savedBook, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@DeleteMapping("/books/delete/{id}")
	public ResponseEntity<Object> deleteBookById(@PathVariable long id) {
		try {
			bookRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e){
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
