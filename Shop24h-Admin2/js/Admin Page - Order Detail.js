const gQUERY_STRING = window.location.search;
const gURL_PARAMS = new URLSearchParams(gQUERY_STRING);
const gId = gURL_PARAMS.get("id") ?? -1;
const gName = gURL_PARAMS.get("name") ?? "";

console.log(gId);
var gPage = 0;
var gResult;
// var gOrderInfo = {
//     status: '',
//     payment: 0,
//     orderDetails = [],
// }
var listBook;

function getDomainName(from = window.location.href) {
    var addr = from;
    var start = addr.indexOf('/') + 2;
    var end = addr.indexOf('/', start);
    return addr.substr(start, end!=-1 ? end - start:addr.length - start);
}

function fillForm(index) {
    $('#book-select').val(gResult.orderDetails[index].book.id);
    $('#quantity-input').val(gResult.orderDetails[index].quantityOrder);
    return false;
}

function updateTable() {
    $("#name-input").val(gName);
    $("#orderid-input").val(gId);
    var orderDetailInfo = "";
    // var start = gPage * gPerPage;
    // var end = Math.min((gPage+1) * gPerPage, pRes.length);
    var i = 1;
    for (let orders of gResult.orderDetails){
        orderDetailInfo += `
        <tr>
            <td>${i}</td>
            <td>${orders.book.bookTitle}</td>
            <td class="text-right">${orders.book.bookPrice.toLocaleString("vi-VN")}đ</td>
            <td>${orders.quantityOrder}</td>
            <td class="text-right">${(orders.book.bookPrice * orders.quantityOrder).toLocaleString("vi-VN")}đ</td>
            <td>
                <a title="Sửa sản phẩm" href="#" onclick="return fillForm(${i-1})">✏</a>
            </td>
        </tr>
        `;
        i++;
    } 
    $("#product-table").html(
        `
        <thead>
                <tr>
                    <th>STT</th>
                    <th>Tên sách</th>
                    <th>Đơn giá</th>
                    <th>Số lượng</th>
                    <th>Thành tiền</th>
                    <th>Action</th>                                                                       
                </tr>
            </thead>
        <tbody>
            ${orderDetailInfo}
            <tr class="font-weight-bold" >
                <td colspan="4">Tổng tiền</td>
                <td class="text-right">${gResult.payment.toLocaleString("vi-VN")}đ</td>
                <td></td>
            </tr>
            <tr class="font-weight-bold" >
                <td colspan="3">Trạng thái</td>
                <td>${gResult.status}</td>
                <td><button class="btn btn-primary" id="btn-confirm">Xác nhận</button></td>
                <td><button class="btn btn-info" id="btn-cancel">Hủy</button></td>
            </tr>
        </tbody>
        `
    );

    $('#btn-confirm').click(function() {
        if (confirm('Xác nhận hóa đơn?')) {
            gResult.status = 'Đã xác nhận';
            saveOrder();
        }
    });

    $('#btn-cancel').click(function() {
        if (confirm('Hủy hóa đơn?')) {
            gResult.status = 'Đã huỷ';
            saveOrder();
        }
    });
}

function saveOrder() {
    $.ajax({
        url: `http://${getDomainName()}:8080/orders/modify/${gId}`,
        type: 'PUT',
        dataType: "json",
        data: JSON.stringify(gResult),
        contentType: "application/json;charset=utf-8",
        success: function(pRes){
            console.log(pRes);
            // gOrderInfo.orderDetails = gResult;
            // gOrderInfo.payment = getTotalPayment();
            updateTable();
            //updatePageSelect(pRes);
        },
        error: function(pAjaxContext){
            console.log(pAjaxContext.responseText);
        }
    })
}

function getTotalPayment() {
    var sum = 0;
    for (let i of gResult.orderDetails) {
        sum += i.book.bookPrice * i.quantityOrder;
    }
    return sum;
}

function getBookById(id) {
    for (let book of listBook) {
        if (id == book.id) return book;
    }
    return null;
}

$(document).ready(function (){
    $.ajax({
        url: `http://${getDomainName()}:8080/orders/find/${gId}`,
        type: 'GET',
        dataType: "json",
        success: function(pRes){
            console.log(pRes);
            gResult = pRes;
            // gOrderInfo.orderDetails = gResult;
            // gOrderInfo.payment = getTotalPayment();
            updateTable();
            //updatePageSelect(pRes);
        },
        error: function(pAjaxContext){
            console.log(pAjaxContext.responseText);
        }
    })

    $.ajax({
        url: `http://${getDomainName()}:8080/books/all`,
        type: 'GET',
        dataType: "json",
        success: function(pRes){
            console.log(pRes);
            listBook = pRes;
            var bookOption = '';
            for (let book of listBook) {
                bookOption += `<option value="${book.id}">${book.id} - ${book.bookTitle}</option>`
            }
            $('#book-select').html(bookOption);
            //updatePageSelect(pRes);
        },
        error: function(pAjaxContext){
            console.log(pAjaxContext.responseText);
        }
    })

    $('#add-button').click(function() {
        var bookId = $('#book-select').val();
        var quantity = $('#quantity-input').val();
        for (let i of gResult.orderDetails) {
            if (i.book.id == bookId) {
                i.quantityOrder += parseInt(quantity);
                i.payment = i.quantityOrder * i.book.bookPrice;
                gResult.payment = getTotalPayment();
                saveOrder();
                return false;
            }
        }
        gResult.orderDetails.push({
            quantityOrder: quantity,
            book: getBookById(bookId)
        });
        gResult.payment = getTotalPayment();
        saveOrder();
        return false;
    });

    $('#update-button').click(function() {
        var bookId = $('#book-select').val();
        var quantity = $('#quantity-input').val();
        for (let i of gResult.orderDetails) {
            if (i.book.id == bookId) {
                i.quantityOrder = parseInt(quantity);
                i.payment = i.quantityOrder * i.book.bookPrice;
                gResult.payment = getTotalPayment();
                saveOrder();
                return false;
            }
        }
        gResult.orderDetails.push({
            quantityOrder: quantity,
            book: getBookById(bookId)
        });
        gResult.payment = getTotalPayment();
        saveOrder();
        return false;
    });
})