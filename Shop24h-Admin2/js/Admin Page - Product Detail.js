const gQUERY_STRING = window.location.search;
const gURL_PARAMS = new URLSearchParams(gQUERY_STRING);
const gId = gURL_PARAMS.get("id") ?? -1;

console.log(gId);
var gResult;

var bookInfo = {
    bookCode: "",
    bookTitle: "",
    author: "",
    bookPrice: 0,
    bookPublisher: "",
    bookType: "",
    quantityInStock: "",
    language: "",
    bookImage: "",
    bookDetailsImage: "",
    shortDescription: "",
    bookDecription: "",
}

function getDomainName(from = window.location.href) {
    var addr = from;
    var start = addr.indexOf('/') + 2;
    var end = addr.indexOf('/', start);
    return addr.substr(start, end!=-1 ? end - start:addr.length - start);
}

function fillForm(){
    $("#book-code").val(gResult.bookCode);
    $("#book-title").val(gResult.bookTitle); 
    $("#book-author").val(gResult.author); 
    $("#book-price").val(gResult.bookPrice); 
    $("#book-publisher").val(gResult.bookPublisher); 
    $("#book-type").val(gResult.bookType); 
    $("#quantity").val(gResult.quantityInStock); 
    $("#language").val(gResult.language); 
    $("#book-image-link").val(gResult.bookImage); 
    $("#book-image-detail-link").val(gResult.bookDetailsImage); 
    $("#short-description").val(gResult.shortDescription); 
    $('#summernote').summernote('code', gResult.bookDecription);
}

function getFormData() {
    bookInfo.bookCode = $("#book-code").val().trim();
    bookInfo.bookTitle = $("#book-title").val().trim(); 
    bookInfo.author = $("#book-author").val().trim(); 
    bookInfo.bookPrice = $("#book-price").val(); 
    bookInfo.bookPublisher = $("#book-publisher").val().trim(); 
    bookInfo.bookType = $("#book-type").val().trim(); 
    bookInfo.quantityInStock = $("#quantity").val().trim(); 
    bookInfo.language = $("#language").val().trim(); 
    bookInfo.bookImage = $("#book-image-link").val().trim(); 
    bookInfo.bookDetailsImage = $("#book-image-detail-link").val().trim(); 
    bookInfo.shortDescription = $("#short-description").val(); 
    bookInfo.bookDecription = $('#summernote').summernote('code');

    if (bookInfo.bookCode == "" || bookInfo.bookTitle == "" ||
    bookInfo.author == "" || bookInfo.bookPrice < 0 ||
    bookInfo.bookPublisher == "" || bookInfo.bookType == "" ||
    bookInfo.quantityInStock < 0 || bookInfo.language == "" ||
    bookInfo.bookImage == "" || bookInfo.bookDetailsImage == ""){
        alert("Nhập sai dữ liệu!");
        return false;
    }
    return true;
}

function getData() {
    $.ajax({
        url: `http://${getDomainName()}:8080/books/${gId}`,
        type: 'GET',
        dataType: "json",
        success: function(pRes){
            console.log(pRes);
            gResult = pRes;
            fillForm();
        },
        error: function(pAjaxContext){
            console.log(pAjaxContext.responseText);
        }
    });
}

function updateBookData(){
    if (!getFormData()) return;
    $.ajax({
        url: `http://${getDomainName()}:8080/books/update/${gId}`,
        type: "PUT",
        dataType: "json",
        data: JSON.stringify(bookInfo),
        contentType: "application/json;charset=utf-8",
        success: function (res) {
            console.log(res);
            alert("Cập nhật dữ liệu thành công");
            // window.location.href = "Admin page - Product List.html";
        },
        error: function (err) {
            console.log(err.response);
            alert(err.response);
        }
    })
}

function addNewBookData(){
    if (!getFormData()) return;
    $.ajax({
        url: `http://${getDomainName()}:8080/books/add`,
        type: "POST",
        dataType: "json",
        data: JSON.stringify(bookInfo),
        contentType: "application/json;charset=utf-8",
        success: function (res) {
            console.log(res);
            alert("Tạo mới dữ liệu thành công");
            // window.location.href = "Admin page - Product List.html";
        },
        error: function (err) {
            console.log(err.response);
            alert(err.response);
        }
    })
}

$(document).ready(function() {
    $('#summernote').summernote({
        placeholder: 'Hello Bootstrap 4',
        tabsize: 4,
        height: 250
    });
    getData();

    $("#save-button").on("click",function(){
        if (gId == -1){
            addNewBookData();
            //window.location.href = "Admin page - Product List.html";
        } else {
            updateBookData();
           // window.location.href = "Admin page - Product List.html";
        }
    }
    )//$('#summernote').summernote('code');
})