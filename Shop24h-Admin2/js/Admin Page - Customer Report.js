//-------------
//- BAR CHART -
//-------------
var now = new Date();
var selectedMonth = now.getMonth() + 1;
var selectedYear = now.getFullYear();
var gRankResult;
var gOrderResult;
var rankBarChartCanvas = $('#monthBarChart').get(0).getContext('2d');
var orderBarChartCanvas = $('#yearBarChart').get(0).getContext('2d');
var rankBarChartData = {
  labels: ['Đang tải...'],
  datasets: [
    {
      label: 'Số khách hàng',
      backgroundColor: 'rgba(60,141,188,0.9)',
      borderColor: 'rgba(60,141,188,0.8)',
      pointRadius: false,
      pointColor: '#3b8bba',
      pointStrokeColor: 'rgba(60,141,188,1)',
      pointHighlightFill: '#fff',
      pointHighlightStroke: 'rgba(60,141,188,1)',
      data: [0]
    },
  ]
};

var orderBarChartData = {
  labels: ['Đang tải...'],
  datasets: [
    {
      label: 'Số khách hàng',
      backgroundColor: 'rgba(60,141,188,0.9)',
      borderColor: 'rgba(60,141,188,0.8)',
      pointRadius: false,
      pointColor: '#3b8bba',
      pointStrokeColor: 'rgba(60,141,188,1)',
      pointHighlightFill: '#fff',
      pointHighlightStroke: 'rgba(60,141,188,1)',
      data: [0]
    },
  ]
};

var barChartOptions = {
  responsive: true,
  maintainAspectRatio: false,
  datasetFill: false,
  scales: {
    yAxes: [{
        ticks: {
            beginAtZero: true
        }
    }]
}
};

var rankChart = new Chart(rankBarChartCanvas, {
  type: 'bar',
  data: rankBarChartData,
  options: barChartOptions
});

var orderChart = new Chart(orderBarChartCanvas, {
  type: 'bar',
  data: orderBarChartData,
  options: barChartOptions
});

function getDomainName(from = window.location.href) {
  var addr = from;
  var start = addr.indexOf('/') + 2;
  var end = addr.indexOf('/', start);
  return addr.substr(start, end!=-1 ? end - start:addr.length - start);
}

function updateRankChart() {
  var labelList = [];
  var dataList = [];
  for (let i of gRankResult) {
    labelList.push(i.name);
    dataList.push(i.count);
  }
  rankChart.config.data.labels = labelList;
  rankChart.config.data.datasets[0].data = dataList;
  rankChart.update();
}

function updateOrderChart() {
  var labelList = [];
  var dataList = [];
  for (let i of gOrderResult) {
    labelList.push(i.name);
    dataList.push(i.count);
  }
  orderChart.config.data.labels = labelList;
  orderChart.config.data.datasets[0].data = dataList;
  orderChart.update();
}

function getRankData() {
  $.ajax({
    url: `http://${getDomainName()}:8080/customer/report/rank`,
    type: 'GET',
    dataType: "json",
    success: function (pRes) {
      console.log(pRes);
      gRankResult = pRes;
      updateRankChart();
      
    },
    error: function (pAjaxContext) {
      console.log(pAjaxContext.responseText);
    }
  })
}

function getOrderData() {
  $.ajax({
    url: `http://${getDomainName()}:8080/customer/report/order`,
    type: 'GET',
    dataType: "json",
    success: function (pRes) {
      console.log(pRes);
      gOrderResult = pRes;
      updateOrderChart();
    },
    error: function (pAjaxContext) {
      console.log(pAjaxContext.responseText);
    }
  })
}

$(document).ready(function () {
  $('#monthly-report').val(selectedMonth);
  getRankData();
  getOrderData();
  var yearSelect = '';
  for (let i=2010;i<=now.getFullYear();i++) {
    yearSelect += `<option value="${i}">${i}</option>`;
  }
  $('#yearly-report').html(yearSelect);
  $('#yearly-report').val(selectedYear);
  $('#monthly-report').change(function() {
    selectedMonth = $(this).val();
    getRankData();
  });
  $('#yearly-report').change(function() {
    selectedYear = $(this).val();
    getOrderData();
  });
  
  $('#btn-export-order').click(function() {
    var todate = new Date($('#date-to').val());
    todate.setDate(todate.getDate() + 1);
    window.location.href = `http://${getDomainName()}:8080/export/order?from=${$('#date-from').val()}&to=${todate.getFullYear()}-${todate.getMonth() + 1}-${todate.getDate()}`;
  });
});
