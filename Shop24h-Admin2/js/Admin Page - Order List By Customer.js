const gQUERY_STRING = window.location.search;
const gURL_PARAMS = new URLSearchParams(gQUERY_STRING);
const gId = gURL_PARAMS.get("id") ?? -1;
const gName = gURL_PARAMS.get("name") ?? "";
const gPhone = gURL_PARAMS.get("phone") ?? "";

console.log(gId);
var gPage = 0;
var gResult;
const gPerPage = 10;

function getDomainName(from = window.location.href) {
    var addr = from;
    var start = addr.indexOf('/') + 2;
    var end = addr.indexOf('/', start);
    return addr.substr(start, end!=-1 ? end - start:addr.length - start);
}

function updateTable(pRes) {
    $("#name-input").val(gName);
    $("#phone-input").val(gPhone);
    var orderInfo = "";
    var start = gPage * gPerPage;
    var end = Math.min((gPage+1) * gPerPage, pRes.length);
    for (let i=start;i<end;i++){
        let orders = pRes[i];
        let orderDate = new Date(orders.orderDate);
        orderInfo += `
        <tr>
            <td>${i+1}</td>
            <td>${orderDate.toLocaleString()}</td>
            <td>${orders.comments}</td>
            <td class="text-right">${orders.payment.toLocaleString("vi-VN")}đ</td>
            <td>${orders.status}</td>
            <td>
                <a title="Xem chi tiet hoa don" href="Admin page - Order Detail.html?id=${orders.id}&name=${gName}">📋</a>
            </td>
        </tr>
            `;
    } 
    $("#product-table").html(
        `
        <thead>
                <tr>
                    <th>STT</th>
                    <th>Ngày order</th>
                    <th>Comment</th>
                    <th>Thanh toán</th>
                    <th>Trạng thái</th>
                    <th>Action</th>                                                                        
                </tr>
            </thead>
        <tbody>
            ${orderInfo}
        </tbody>
        `
    );
}

function updatePageSelect(pRes) {
    $('#page-button').html('');
    pButton = '';
    pageCount = Math.ceil(pRes.length / gPerPage);
    for (let i=1;i<=pageCount;i++) {
        if (i == gPage+1) pButton += `<button class="btn btn-primary">${i}</button>`;
        else pButton += `<button onclick="setPage(${i-1})" class="btn btn-light">${i}</button>`;
    }
    $('#page-button').html(`
        <button id="prev-page" class="btn btn-primary">«</button>
        ${pButton}
        <button id="next-page" class="btn btn-primary">»</button>
    `);
    $('#prev-page').click(function() {
        if (gPage > 0) {
            setPage(gPage - 1);
        };
        console.log(gPage);
    });
    $('#next-page').click(function() {
        if (gPage + 1 < pageCount) {
            setPage(gPage + 1);
        };
        console.log(gPage);
    });
}

function setPage(num) {
    gPage = num;
    updatePageSelect(gResult);
    updateTable(gResult);
}

$(document).ready(function (){
    $.ajax({
        url: `http://${getDomainName()}:8080/users/${gId}/orders`,
        type: 'GET',
        dataType: "json",
        success: function(pRes){
            console.log(pRes);
            gResult = pRes;
            updateTable(pRes);
            updatePageSelect(pRes);
        },
        error: function(pAjaxContext){
            console.log(pAjaxContext.responseText);
        }
    })
})