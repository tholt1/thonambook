const gQUERY_STRING = window.location.search;
const gURL_PARAMS = new URLSearchParams(gQUERY_STRING);
const gId = gURL_PARAMS.get("id") ?? -1;

console.log(gId);
var gResult;

var usersInfo = {
    address: "",
    cityState: "",
    author: "",
    region: "",
    fullName: "",
    phoneNumber: "",
    payment: 0,
    email: "",
    userName: "",
    password: "",
    role: 0,
    register: 0,
    orders: []
}

function getDomainName(from = window.location.href) {
    var addr = from;
    var start = addr.indexOf('/') + 2;
    var end = addr.indexOf('/', start);
    return addr.substr(start, end!=-1 ? end - start:addr.length - start);
}

function fillForm(){
    $("#name-input").val(gResult.fullName);
    $("#email-input").val(gResult.email); 
    $("#phone-input").val(gResult.phoneNumber); 
    $("#region-input").val(gResult.region); 
    $("#city-state-input").val(gResult.cityState); 
    $("#payment-input").val(gResult.payment); 
    $("#address-input").val(gResult.address); 
}

function getFormData() {
    usersInfo.fullName =  $("#name-input").val().trim();
    usersInfo.email = $("#email-input").val().trim(); 
    usersInfo.phoneNumber = $("#phone-input").val().trim(); 
    usersInfo.region = $("#region-input").val().trim(); 
    usersInfo.cityState = $("#city-state-input").val().trim(); 
    usersInfo.payment = $("#payment-input").val(); 
    usersInfo.userName = $("#address-input").val().trim();
    usersInfo.password = $("#address-input").val().trim();
    usersInfo.role = $("#address-input").val();
    usersInfo.register = $("#address-input").val();

    
    if (usersInfo.fullName == "" || usersInfo.email == "" || usersInfo.email.indexOf("@") == -1 ||
        usersInfo.phoneNumber == "" || usersInfo.region == "" || usersInfo.cityState == "" || 
        usersInfo.payment < 0 || usersInfo.address == ""){
            alert("Nhập sai dữ liệu!");
            return false;
        }
        return false;
}

function getData() {
    $.ajax({
        url: `http://${getDomainName()}:8080/users/${gId}`,
        type: 'GET',
        dataType: "json",
        success: function(pRes){
            console.log(pRes);
            gResult = pRes;
            fillForm();
        },
        error: function(pAjaxContext){
            console.log(pAjaxContext.responseText);
        }
    });
}

function updateCustomerData(){
    if(!getFormData()) return;
    $.ajax({
        url: `http://${getDomainName()}:8080/users/update/${gId}`,
        type: "PUT",
        dataType: "json",
        data: JSON.stringify(usersInfo),
        contentType: "application/json;charset=utf-8",
        success: function (res) {
            console.log(res);
            alert("Cập nhật dữ liệu thành công");
            // window.location.href = "Admin page - Product List.html";
        },
        error: function (err) {
            console.log(err.response);
            alert(err.response);
        }
    })
}

function addNewCustomerData(){
    if(!getFormData()) return;
    $.ajax({
        url: "http://${getDomainName()}:8080/users/add",
        type: "POST",
        dataType: "json",
        data: JSON.stringify(usersInfo),
        contentType: "application/json;charset=utf-8",
        success: function (res) {
            console.log(res);
            alert("Tạo mới dữ liệu thành công");
            // window.location.href = "Admin page - Product List.html";
        },
        error: function (err) {
            console.log(err.response);
            alert(err.response);
        }
    })
}

$(document).ready(function() {
    getData();

    $("#save-button").on("click",function(){
        if (gId == -1){
            addNewCustomerData();
            //window.location.href = "Admin page - Product List.html";
        } else {
            updateCustomerData();
           // window.location.href = "Admin page - Product List.html";
        }
    }
    )//$('#summernote').summernote('code');
})