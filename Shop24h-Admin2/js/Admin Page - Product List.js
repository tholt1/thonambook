var gPage = 0;
var gResult;
const gPerPage = 10;

function getDomainName(from = window.location.href) {
    var addr = from;
    var start = addr.indexOf('/') + 2;
    var end = addr.indexOf('/', start);
    return addr.substr(start, end!=-1 ? end - start:addr.length - start);
}

function updateTable(pRes) {
    var product = "";
    var i = 1;
    for (let book of gResult.data){
        product += `
        <tr>
            <td>${i}</td>
            <td><img class="product-img" src="${book.bookImage}"></td>
            <td>${book.bookTitle}</td>
            <td>${book.bookCode}</td>
            <td>${book.author}</td>
            <td>${book.bookPublisher}</td>
            <td>${book.bookPrice}</td>
            <td>${book.quantityInStock}</td>
            <td><a href="Admin page - Product Detail.html?id=${book.id}">✍</a><a href="#" onclick="return deleteBook(${i-1})">✖</a></td>
        </tr>
        `;
        i++;
    }
    $("#product-table").html(`
        <thead>
            <tr>
                <th>STT</th>
                <th>Ảnh</th>
                <th>Tên sách</th>
                <th>bookCode</th>
                <th>Tác giả</th>
                <th>NXB</th>
                <th>Giá</th>
                <th>Số lượng</th>                                                                               
                <th>Action</th>                                                                               
            </tr>
        </thead>
        <tbody>
            ${product}
        </tbody>
    `);
}

function deleteBook(index) {
    if (confirm('Bạn có chắc muốn xóa sản phẩm này khỏi danh sách ?')) {
        $.ajax({
            url: `http://${getDomainName()}:8080/books/delete/${gResult.data[index].id}`,
            type: 'DELETE',
            success: function(pRes){
                console.log(`Delete book id ${gResult.data[index].id} success`);
                getData();
            },
            error: function(pAjaxContext){
                console.log(pAjaxContext.responseText);
            }
        });
    }
    return false;
}

function updatePageSelect(pRes) {
    $('#page-button').html('');
    pButton = '';
    pageCount = gResult.pageCount;
    for (let i=1;i<=pageCount;i++) {
        if (i == gPage+1) pButton += `<button class="btn btn-primary">${i}</button>`;
        else pButton += `<button onclick="setPage(${i-1})" class="btn btn-light">${i}</button>`;
    }
    $('#page-button').html(`
        <button id="prev-page" class="btn btn-primary">«</button>
        ${pButton}
        <button id="next-page" class="btn btn-primary">»</button>
    `);
    $('#prev-page').click(function() {
        if (gPage > 0) setPage(gPage - 1);
    });
    $('#next-page').click(function() {
        if (gPage + 1 < pageCount) setPage(gPage + 1);
    });
}

function setPage(num) {
    gPage = num;
    getData();
}

function getData() {
    $.ajax({
        url: `http://${getDomainName()}:8080/books/filter?page=${gPage + 1}`,
        type: 'GET',
        dataType: "json",
        success: function(pRes){
            console.log(pRes);
            gResult = pRes;
            updateTable(pRes);
            updatePageSelect(pRes);
        },
        error: function(pAjaxContext){
            console.log(pAjaxContext.responseText);
        }
    });
}

$(document).ready(function (){
    getData();
})