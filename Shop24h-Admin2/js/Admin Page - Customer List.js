var gPage = 0;
var gResult;
const gPerPage = 10;

function getDomainName(from = window.location.href) {
    var addr = from;
    var start = addr.indexOf('/') + 2;
    var end = addr.indexOf('/', start);
    return addr.substr(start, end!=-1 ? end - start:addr.length - start);
}

function updateTable(pRes) {
    var usersInfo = "";
    var start = gPage * gPerPage;
    var end = Math.min((gPage+1) * gPerPage, pRes.length);
    for (let i=start;i<end;i++){
        let users = pRes[i];
        usersInfo += `
        <tr>
            <td>${i+1}</td>
            <td>${users.fullName}</td>
            <td>${users.phoneNumber}</td>
            <td>${users.email}</td>
            <td>${users.region}</td>
            <td>${users.cityState}</td>
            <td>${users.address}</td>
            <td class="text-right">${users.payment.toLocaleString("vi-VN")}đ</td>
            <td>${users.userName}</td>
            <td>${users.role}</td>
            <td>${users.register}</td>
            <td>
                <a href="Admin page - Customer Detail.html?id=${users.id}">✍</a>
                <a href="Admin page - Order List By Customer.html?id=${users.id}&name=${users.fullName}&phone=${users.phoneNumber}">📋</a>
            </td>
        </tr>
            `;
    } 
    $("#product-table").html(
        `
        <thead>
                <tr>
                    <th>STT</th>
                    <th>Họ và tên</th>
                    <th>SĐT</th>
                    <th>email</th>
                    <th>Tỉnh</th>
                    <th>TP/Huyện</th>
                    <th>Địa chỉ</th>
                    <th>Thanh toán</th>
                    <th>UserName</th>
                    <th>Vai trò</th>
                    <th>Đăng ký</th>
                    <th>Action</th>                                                                               
                </tr>
            </thead>
        <tbody>
            ${usersInfo}
        </tbody>
        `
    );
}

function updatePageSelect(pRes) {
    $('#page-button').html('');
    pButton = '';
    pageCount = Math.ceil(pRes.length / gPerPage);
    for (let i=1;i<=pageCount;i++) {
        if (i == gPage+1) pButton += `<button class="btn btn-primary">${i}</button>`;
        else pButton += `<button onclick="setPage(${i-1})" class="btn btn-light">${i}</button>`;
    }
    $('#page-button').html(`
        <button id="prev-page" class="btn btn-primary">«</button>
        ${pButton}
        <button id="next-page" class="btn btn-primary">»</button>
    `);
    $('#prev-page').click(function() {
        if (gPage > 0) {
            setPage(gPage - 1);
        };
        console.log(gPage);
    });
    $('#next-page').click(function() {
        if (gPage + 1 < pageCount) {
            setPage(gPage + 1);
        };
        console.log(gPage);
    });
}

function setPage(num) {
    gPage = num;
    updatePageSelect(gResult);
    updateTable(gResult);
}

$(document).ready(function (){
    $.ajax({
        url: `http://${getDomainName()}:8080/users/all`,
        type: 'GET',
        dataType: "json",
        success: function(pRes){
            console.log(pRes);
            gResult = pRes;
            updateTable(pRes);
            updatePageSelect(pRes);
        },
        error: function(pAjaxContext){
            console.log(pAjaxContext.responseText);
        }
    })
})