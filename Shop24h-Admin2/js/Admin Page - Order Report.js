//-------------
//- BAR CHART -
//-------------
var now = new Date();
var selectedMonth = now.getMonth() + 1;
var selectedYear = now.getFullYear();
var gMonthResult;
var gYearResult;
var monthBarChartCanvas = $('#monthBarChart').get(0).getContext('2d');
var yearBarChartCanvas = $('#yearBarChart').get(0).getContext('2d');
var monthBarChartData = {
  labels: ['Đang tải...'],
  datasets: [
    {
      label: 'Doanh thu',
      backgroundColor: 'rgba(60,141,188,0.9)',
      borderColor: 'rgba(60,141,188,0.8)',
      pointRadius: false,
      pointColor: '#3b8bba',
      pointStrokeColor: 'rgba(60,141,188,1)',
      pointHighlightFill: '#fff',
      pointHighlightStroke: 'rgba(60,141,188,1)',
      data: [0]
    },
  ]
};

var yearBarChartData = {
  labels: ['Đang tải...'],
  datasets: [
    {
      label: 'Doanh thu',
      backgroundColor: 'rgba(60,141,188,0.9)',
      borderColor: 'rgba(60,141,188,0.8)',
      pointRadius: false,
      pointColor: '#3b8bba',
      pointStrokeColor: 'rgba(60,141,188,1)',
      pointHighlightFill: '#fff',
      pointHighlightStroke: 'rgba(60,141,188,1)',
      data: [0]
    },
  ]
};

var barChartOptions = {
  responsive: true,
  maintainAspectRatio: false,
  datasetFill: false,
  scales: {
    yAxes: [{
        ticks: {
            beginAtZero: true
        }
    }]
}
};

var monthChart = new Chart(monthBarChartCanvas, {
  type: 'bar',
  data: monthBarChartData,
  options: barChartOptions
});

var yearChart = new Chart(yearBarChartCanvas, {
  type: 'bar',
  data: yearBarChartData,
  options: barChartOptions
});

function getDomainName(from = window.location.href) {
  var addr = from;
  var start = addr.indexOf('/') + 2;
  var end = addr.indexOf('/', start);
  return addr.substr(start, end!=-1 ? end - start:addr.length - start);
}

function updateMonthChart() {
  var labelList = [];
  var dataList = [];
  for (let i of gMonthResult) {
    labelList.push(i.date);
    dataList.push(i.payment);
  }
  monthChart.config.data.labels = labelList;
  monthChart.config.data.datasets[0].data = dataList;
  monthChart.update();
}

function updateYearChart() {
  var labelList = [];
  var dataList = [];
  for (let i of gYearResult) {
    labelList.push(`${i.month}/${i.year}`);
    dataList.push(i.payment);
  }
  yearChart.config.data.labels = labelList;
  yearChart.config.data.datasets[0].data = dataList;
  yearChart.update();
}

function getMonthData() {
  $.ajax({
    url: `http://${getDomainName()}:8080/report/month/${selectedMonth}`,
    type: 'GET',
    dataType: "json",
    success: function (pRes) {
      console.log(pRes);
      gMonthResult = pRes;
      updateMonthChart();
      
    },
    error: function (pAjaxContext) {
      console.log(pAjaxContext.responseText);
    }
  })
}

function getYearData() {
  $.ajax({
    url: `http://${getDomainName()}:8080/report/year/${selectedYear}`,
    type: 'GET',
    dataType: "json",
    success: function (pRes) {
      console.log(pRes);
      gYearResult = pRes;
      updateYearChart();
    },
    error: function (pAjaxContext) {
      console.log(pAjaxContext.responseText);
    }
  })
}

$(document).ready(function () {
  $('#monthly-report').val(selectedMonth);
  getMonthData();
  getYearData();
  var yearSelect = '';
  for (let i=2010;i<=now.getFullYear();i++) {
    yearSelect += `<option value="${i}">${i}</option>`;
  }
  $('#yearly-report').html(yearSelect);
  $('#yearly-report').val(selectedYear);
  $('#monthly-report').change(function() {
    selectedMonth = $(this).val();
    getMonthData();
  });
  $('#yearly-report').change(function() {
    selectedYear = $(this).val();
    getYearData();
  });
  
  $('#btn-export-order').click(function() {
    var todate = new Date($('#date-to').val());
    todate.setDate(todate.getDate() + 1);
    window.location.href = `http://${getDomainName()}:8080/export/order?from=${$('#date-from').val()}&to=${todate.getFullYear()}-${todate.getMonth() + 1}-${todate.getDate()}`;
  });
});
