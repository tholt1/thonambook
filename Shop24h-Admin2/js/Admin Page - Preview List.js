var gPage = 0;
var gResult;
const gPerPage = 10;

function getDomainName(from = window.location.href) {
    var addr = from;
    var start = addr.indexOf('/') + 2;
    var end = addr.indexOf('/', start);
    return addr.substr(start, end!=-1 ? end - start:addr.length - start);
}

function getStarFromValue(num) {
    ratingStar = ''
    for (var i = 1; i <= 5; i++) {
        if (i > num) ratingStar += `☆`;
        else ratingStar += `★`;
    }
    return ratingStar;
}

function deleteCommentButton(id) {
    var vIsConfirm = confirm("Bạn có chắc muốn xóa id này?");
    if (vIsConfirm) {
        $.ajax({
            url: `http://${getDomainName()}:8080/ratingcomment/delete/${id}`,
            type: 'DELETE',
            success: function (pRes) {
                console.log(pRes);
                location.reload();
            },
            error: function (pAjaxContext) {
                console.log(pAjaxContext.responseText);
            }
        })
    }
}

function updateTable(pRes) {
    var previewsInfo = "";
    var start = gPage * gPerPage;
    var end = Math.min((gPage + 1) * gPerPage, pRes.length);
    for (let i = start; i < end; i++) {
        let previews = pRes[i];
        previewsInfo += `
        <tr>
            <td>${i + 1}</td>
            <td>${previews.book.id}</td>
            <td>${previews.book.bookTitle}</td>
            <td>${previews.users.userName}</td>
            <td>${previews.users.fullName}</td>
            <td>${new Date(previews.dateCreated).toLocaleString("vi-VN")}</td>
            <td style="color: chartreuse">${getStarFromValue(previews.rating)}</td>
            <td>${previews.comment}</td>
            <td>
                <a class="text-danger" onclick="deleteCommentButton(${previews.id})">✕</a>
            </td>
        </tr>
            `;
    }
    $("#preview-table").html(
        `
        <thead>
                <tr>
                    <th>STT</th>
                    <th>BookId</th>
                    <th>Tên sách</th>
                    <th>UserName</th>
                    <th>Họ và tên</th>
                    <th>Ngày tạo</th>
                    <th>Đánh giá</th>
                    <th>Bình Luận</th>
                    <th>Action</th>                                                                               
                </tr>
            </thead>
        <tbody>
            ${previewsInfo}
        </tbody>
        `
    );
}

function updatePageSelect(pRes) {
    $('#page-button').html('');
    pButton = '';
    pageCount = Math.ceil(pRes.length / gPerPage);
    for (let i = 1; i <= pageCount; i++) {
        if (i == gPage + 1) pButton += `<button class="btn btn-primary">${i}</button>`;
        else pButton += `<button onclick="setPage(${i - 1})" class="btn btn-light">${i}</button>`;
    }
    $('#page-button').html(`
        <button id="prev-page" class="btn btn-primary">«</button>
        ${pButton}
        <button id="next-page" class="btn btn-primary">»</button>
    `);
    $('#prev-page').click(function () {
        if (gPage > 0) {
            setPage(gPage - 1);
        };
        console.log(gPage);
    });
    $('#next-page').click(function () {
        if (gPage + 1 < pageCount) {
            setPage(gPage + 1);
        };
        console.log(gPage);
    });
}

function setPage(num) {
    gPage = num;
    updatePageSelect(gResult);
    updateTable(gResult);
}

$(document).ready(function () {
    $.ajax({
        url: `http://${getDomainName()}:8080/ratingcomment/all`,
        type: 'GET',
        dataType: "json",
        success: function (pRes) {
            console.log(pRes);
            gResult = pRes;
            updateTable(pRes);
            updatePageSelect(pRes);
        },
        error: function (pAjaxContext) {
            console.log(pAjaxContext.responseText);
        }
    })
})